﻿using SAPbouiCOM.Framework;
using Serilog;
using SSACommon;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace SSAINFO_ELMA
{
    public static class MenuInfo
    {
        private static string XmlMenuAdd;
        private static string XmlMenuRemove;

        private static List<SSACommon.AddonInfo> m_lstAddon;

        /// <summary>
        /// Aggiunta menù sulla barra destra, all'interno dei moduli. Per i dettagli:
        /// Per compilare il menu, sostituire i titoli dei nodi a piacimento. Per quanto riguarda il padre, esso è indicabile aggiungendo come FatherUID il numero
        /// identificativo del menu, ottenibile andando su SAP B1, abilitando informazioni di sistema, selezionando con il mouse il menù a tendina "moduli" e
        /// leggendo quello che viene indicato nella barra inferiore.
        /// </summary>
        public static void AddMenu()
        {
            try
            {
                System.Xml.XmlDocument oXmlDoc = default(System.Xml.XmlDocument);
                string sImgPath = null;

                sImgPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location); //System.Windows.Forms.Application.StartupPath;
                //sImgPath = sImgPath.Remove(sImgPath.Length - 9, 9);
                sImgPath += "\\img\\icon.png";


                oXmlDoc = new System.Xml.XmlDocument();

                oXmlDoc.Load(SSACommon.Util.GetXmlStream(typeof(MenuInfo), "Xml.Menu.ADDON_NAME_mnuMain.xml"));
                XmlMenuAdd = oXmlDoc.InnerXml;
                //modifico la posizione dell'immagine in funzione del percorso di installazione sul client
                XmlMenuAdd = XmlMenuAdd.Replace("Image=\"[position]\"", "Image=\"" + sImgPath + "\"");
                XmlMenuRemove = oXmlDoc.InnerXml.ToLower().Replace("<action type=\"add\">", "<action type=\"remove\">");

                SSACommon.AppInfo.Application.LoadBatchActions(XmlMenuAdd);
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore inizializzazione menu moduli, messaggio: {ex.Message}");
            }

        }

        /// <summary>
        /// Metodo alternativo per aggiungere dei menu nella barra dei moduli. Da chiamare dap Program.cs tramite MyMenu.AddMenuItems();
        /// </summary>
        public static void AddMenuItems()
        {
            try
            {
                SAPbouiCOM.Menus oMenus = null;
                SAPbouiCOM.MenuItem oMenuItem = null;

                oMenus = Application.SBO_Application.Menus;

                SAPbouiCOM.MenuCreationParams oCreationPackage = null;
                oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
                oMenuItem = Application.SBO_Application.Menus.Item("43520"); // moudles'

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "SSAINFO_ELMA";
                oCreationPackage.String = "SSAINFO_ELMA";
                oCreationPackage.Enabled = true;
                oCreationPackage.Position = -1;

                oMenus = oMenuItem.SubMenus;


                oMenus.AddEx(oCreationPackage);
                // Get the menu collection of the newly added pop-up item
                oMenuItem = Application.SBO_Application.Menus.Item("SSAINFO_ELMA");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "SSAINFO_ELMA.Forms.Form1";
                oCreationPackage.String = "Form1";
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore inizializzazione menu moduli, messaggio: {ex.Message}");
                Application.SBO_Application.SetStatusBarMessage("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, true);

            }
        }

        /// <summary>
        /// Menu per aggiungere il collegamento al file di log.
        /// </summary>
        public static void AddHelpMenu()
        {
            try
            {
                SAPbouiCOM.Menus oMenus = null;
                SAPbouiCOM.MenuItem oMenuItem = null;

                oMenus = AppInfo.Application.Menus;
                if (!oMenus.Exists("SSAINFO_ELMA_Log"))
                {
                    SAPbouiCOM.MenuCreationParams oCreationPackage = null;
                    oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(AppInfo.Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
                    oMenuItem = AppInfo.Application.Menus.Item("278"); // Help -> Support Desk
                    oMenus = oMenuItem.SubMenus;
                    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                    oCreationPackage.UniqueID = "SSAINFO_ELMA_Log";
                    oCreationPackage.String = "add-on SSAINFO_ELMA Log File";
                    oCreationPackage.Enabled = true;
                    oMenus.AddEx(oCreationPackage);
                }
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore inizializzazione menu, messaggio: {ex.Message}");
            }
        }

        public static void loadAddonList(List<SSACommon.AddonInfo> lstAddon)
        {
            m_lstAddon = lstAddon;
        }

        /// <summary>
        /// Gestore eventi di chiamata ai menu
        /// </summary>
        /// <param name="pVal"></param>
        /// <param name="BubbleEvent"></param>
        public static void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction)
                {
                    switch (pVal.MenuUID)
                    {

                        case "SSAINFO_ELMA_Log":
                            string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                            string configvalue = ConfigurationManager.AppSettings["serilog:write-to:File.path"];
                            configvalue = configvalue.Remove(configvalue.Length - 4, 4);
                            string filePath = configvalue.Replace("%APPDATA%", appDataPath) + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                            System.Diagnostics.Process.Start(filePath);
                            break;
                        case "SSAINFO_ELMA_mnu_0101":
                            //Pressione menu barra di sinistra, evento da gestire qui. Tutti gli altri hanno un flusso simile
                            Utility.Tools.ImportDatiXLS_SSAINFO_LISALTLAR();
                            break;
                        case "xxxx": // qui vanno i nuovi menu
                            //new Form.FormActivities().Show();
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                AppInfo.Application.MessageBox(ex.ToString(), 1, "Ok", "", "");
                Utility.Tools.LogError(ex, $"Errore inizializzazione menu, messaggio: {ex.Message}");
            }
        }

        public static void RemoveMenu()
        {
            AppInfo.Application.LoadBatchActions(XmlMenuRemove);
        }

        public static void RemoveContextMenu(string MenuID)
        {
            try
            {
                if (AppInfo.Application.Menus.Exists(MenuID) == true)
                    AppInfo.Application.Menus.RemoveEx(MenuID);
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore rimozione menu, messaggio: {ex.Message}");
            }
        }

        public static void CreateContextMenu(SAPbouiCOM.MenuItem oMenuItem, SAPbouiCOM.BoMenuType boMenuType,
          string MenuID, string MenuString, bool bEnabled = true, int Position = -1)
        {
            SAPbouiCOM.Menus oMenus;
            SAPbouiCOM.MenuCreationParams oCreationPackage;

            try
            {
                oMenus = oMenuItem.SubMenus;
                oCreationPackage = (SAPbouiCOM.MenuCreationParams)AppInfo.Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                oCreationPackage.Type = boMenuType;
                oCreationPackage.UniqueID = MenuID;
                oCreationPackage.String = MenuString;
                oCreationPackage.Enabled = bEnabled;
                if (Position == -1)
                    oCreationPackage.Position = oMenus.Count + 1;
                else
                    oCreationPackage.Position = Position;

                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore creazione menu, messaggio: {ex.Message}");
            }
        }

        public static void ClearMenu(string menuUniqueID)
        {
            try
            {
                //SSAINFO_OT_mnuBase
                XDocument xDoc;

                SAPbouiCOM.Menus oMenus;
                oMenus = AppInfo.Application.Menus;

                string xmlMenu = oMenus.GetAsXML();

                xDoc = XDocument.Parse(xmlMenu);
                IEnumerable<XElement> rootMenu = (from el in xDoc.Descendants("action").Elements("Menu")
                                                  where (string)el.Attribute("UniqueID") == menuUniqueID
                                                  select el);


                XElement xElementRemove = new XElement("Application",
                    new XElement("Menus",
                        new XElement("action", new XAttribute("type", "remove")
                        , rootMenu)));

                string xmlMenuRemove = String.Concat(xElementRemove);

                AppInfo.Application.LoadBatchActions(xmlMenuRemove);
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore pulizia menu, messaggio: {ex.Message}");
            }
        }
    }
}
