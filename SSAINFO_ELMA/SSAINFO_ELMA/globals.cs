﻿using Serilog;
using SSACommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAINFO_ELMA
{
    public static class CompanyInfo
    {
        public static SAPbobsCOM.Company Company;
    }

    public static class AppInfo
    {
        public static SAPbouiCOM.Application Application;
    }

    public static class AddonInfo
    {
        public static string AddonCode = "SSAINFO_ELMA";
        public static string AddonName = "SSAINFO_ELMA";
        public static string AddonDescription = "ADDON SSAINFO_ELMA";
        public static string AddonProvider = "SSAINFORMATICA";
        public static int AddonDBVersion = 0; //da questa info vengono eseguiti gli script per l'aggiornamento delle procedure SQL sul DB; corrisponde con il massivo della versione Vxxxx nella cartella DBVersions.MSSQL.dbUpdates
    }

    public sealed class AdminInfo
    {
        static readonly AdminInfo _instance = new AdminInfo();

        static private SAPbobsCOM.CompanyService oCompServ = null;
        static private SAPbobsCOM.AdminInfo oAdminInfo;

        public static AdminInfo Instance
        {
            get
            {
                return _instance;
            }
        }

        AdminInfo()
        {
            try
            {
                oCompServ = (SAPbobsCOM.CompanyService)CompanyInfo.Company.GetCompanyService();
                oAdminInfo = oCompServ.GetAdminInfo();
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore creazione istanza, messaggio: {ex.Message}");
                AppInfo.Application.StatusBar.SetText(ex.Message
                    , SAPbouiCOM.BoMessageTime.bmt_Short
                    , SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }

        }

        public static SAPbobsCOM.AdminInfo GetAdminInfo()
        {

            return oAdminInfo;
        }

        public static string CompanyName()
        {
            return oAdminInfo.CompanyName;
        }

        public static SAPbobsCOM.CompanyService CompanyService
        {
            get
            {
                if (oCompServ == null)
                    oCompServ = (SAPbobsCOM.CompanyService)CompanyInfo.Company.GetCompanyService();
                return oCompServ;
            }
        }
    }
}
