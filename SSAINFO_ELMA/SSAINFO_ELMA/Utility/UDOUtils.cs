﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAINFO_ELMA.Utility
{
    public static class UDOUtils
    {
        /// <summary>
        /// Return existing UDO object values
        /// </summary>
        /// <param name="udoCode">UDO Code</param>
        /// <param name="propertyKey">Table Key Field</param>
        /// <param name="propertyValue">Table Key Value</param>
        /// <returns></returns>
        public static SAPbobsCOM.GeneralData GetGeneralDataFromUDO(string udoCode, string propertyKey, string propertyValue)
        {
            SAPbobsCOM.CompanyService oCompanyService = null;
            SAPbobsCOM.GeneralService oGeneralService = null;
            SAPbobsCOM.GeneralDataParams oGeneralParams = null;
            SAPbobsCOM.GeneralData oGeneralData = null;

            try
            {
                oCompanyService = CompanyInfo.Company.GetCompanyService();
                oGeneralService = oCompanyService.GetGeneralService(udoCode);
                oGeneralParams = (SAPbobsCOM.GeneralDataParams)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                oGeneralParams.SetProperty(propertyKey, propertyValue);
                oGeneralData = ((SAPbobsCOM.GeneralData)(oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)));
                oGeneralData = oGeneralService.GetByParams(oGeneralParams);

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            finally
            {
                //completare

            }

            return oGeneralData;

        }

        public static string GetUDOxml(string codeUDO, string tableMaster, string keyField, string keyValue)
        {
            SAPbobsCOM.CompanyService oCompanyService = null;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralDataParams oGeneralParams;
            SAPbobsCOM.GeneralData oGeneralData;

            string sResult = "";
            SAPbobsCOM.Recordset oRS = null;
            string strSQL = "";

            try
            {
                oCompanyService = CompanyInfo.Company.GetCompanyService();
                oRS = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (CompanyInfo.Company.DbServerType == SAPbobsCOM.BoDataServerTypes.dst_HANADB)
                {
                    strSQL = $"SELECT COUNT(1) FROM \"{tableMaster}\" WHERE CAST(\"{keyField}\" as nvarchar(100)) = '{keyValue}'";
                }
                else
                {
                    strSQL = $"SELECT COUNT(1) FROM [{tableMaster}] WHERE CAST(\"{keyField}\" as nvarchar(100)) = '{keyValue}'";
                }

                oRS.DoQuery(strSQL);
                if ((int)oRS.Fields.Item(0).Value > 0)
                {
                    oGeneralService = oCompanyService.GetGeneralService(codeUDO);
                    oGeneralParams = (SAPbobsCOM.GeneralDataParams)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                    oGeneralParams.SetProperty(keyField, keyValue);
                    oGeneralData = ((SAPbobsCOM.GeneralData)(oGeneralService.GetByParams(oGeneralParams)));

                    sResult = oGeneralData.ToXMLString();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oCompanyService != null)
                {
                    SSACommon.Disposer.ReleaseComObject(oCompanyService);
                    oCompanyService = null;

                }
                if (oRS != null)
                {
                    SSACommon.Disposer.ReleaseComObject(oRS);
                    oRS = null;

                }
                GC.Collect();
            }


            return sResult;
        }

    }
}
