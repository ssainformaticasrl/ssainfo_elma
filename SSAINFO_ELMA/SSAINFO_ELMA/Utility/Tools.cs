﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml; //EPPLus
using System.Windows.Forms;
using System.IO;

namespace SSAINFO_ELMA.Utility
{
    public static class Tools
    {
        /// <summary>
        /// Funzione per pulire gli oggetti COM
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="gcClean"></param>
        public static void ObjectRelease(Object obj, bool gcClean = true)
        {
            try
            {
                if (obj != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(obj);
                    obj = null;
                }

                if (gcClean)    // aggiunta per impedire GC.Collect in serie inutili, in modo di attivarlo solo a piacere (solitamente l'ultima chiamata)
                {
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Impossibile pulire l'oggetto COM passata, errore {ex.Message}");
            }
        }

        /// <summary>
        /// Funzione per la stampa degli errori comprendente anche l'innerexception e lo stacktrace
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="gcClean"></param>
        public static void LogError(Exception exception, string message = "")
        {
            try
            {
                if (exception.InnerException != null)
                {
                    Log.ForContext("Message", exception.Message)
                                   .ForContext("Inner Exception Message", exception.InnerException.Message)
                                   .ForContext("Inner Exception Stack Trace", exception.InnerException.StackTrace)
                                   .ForContext("Stack Trace", exception.StackTrace)
                                   .Error(message);
                }
                else
                {
                    Log.ForContext("Message", exception.Message)
                                   .ForContext("Stack Trace", exception.StackTrace)
                                   .Error(message);
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Problema nella scrittura dei messaggi di log... Message: {ex.Message}");
            }
        }

        /// <summary>
        /// generazione codice sha
        /// </summary>
        /// <param name="passedData"></param>
        /// <returns></returns>
        public static string generateSHA(string passedData)
        {
            byte[] salt;
            new System.Security.Cryptography.RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new System.Security.Cryptography.Rfc2898DeriveBytes(passedData, salt, 100000);
            byte[] hashed = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hashed, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }

        /// <summary>
        /// funzione per il controllo della password in hash
        /// </summary>
        /// <param name="passedPassword"></param>
        /// <param name="hash"></param>
        public static bool verifySHA(string passedData, string hash)
        {
            string savedPasswordHash = hash;
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            var pbkdf2 = new Rfc2898DeriveBytes(passedData, salt, 100000);
            byte[] hashed = pbkdf2.GetBytes(20);
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hashed[i])
                //throw new UnauthorizedAccessException();
                {
                    return false;
                }
            return true;
        }


        //Function to get string from end
        public static String Right(String strParam, int iLen)
        {
            if ((iLen > 0) && (iLen < strParam.Length))
                return strParam.Substring(strParam.Length - iLen, iLen);
            else
                return strParam;
        }

        //Function to get string from start
        public static String Left(String strParam, int iLen)
        {
            if ((iLen > 0) && (iLen < strParam.Length))
                return strParam.Substring(0, iLen);
            else
                return strParam;
        }
        public static void ImportDatiXLS_SSAINFO_LISALTLAR()
        {
            SAPbouiCOM.BoStatusBarMessageType mt;
            long lLetti = 0, lModificati = 0, lAggiunti = 0;
            string sValTMP = "";
            try
            {
                SAPbobsCOM.Recordset oRecordSet = null;                
                StreamWriter writer = null;
                SSACommon.OpenFileThread o = new SSACommon.OpenFileThread();
                //o.InitialDirectory = MyDBDataSources("@SSAINFO_BK_QTD1").GetValue(string.Format("U_ATCPATH{0}", Convert.ToInt32(attachmentNumber)), distintaRow);
                System.Windows.Forms.DialogResult res = o.ShowDialog();
                string sFileName = "", sVal = "";
                if (res == System.Windows.Forms.DialogResult.OK)
                {
                    sFileName = o.FileName;
                }
                if (sFileName == "")
                {
                    return;
                }
                switch (MessageBox.Show("Procedere con l'importazione del file: \r\n" + sFileName + " ?", "Richiesta conferma operazione.", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        // "Yes" processing
                        break;
                    case DialogResult.No:
                        // "Cancel" processing                       
                        return;
                        //break;
                }

                mt = SAPbouiCOM.BoStatusBarMessageType.smt_None;
                AppInfo.Application.StatusBar.SetText("Iniziato import listini dal file "+ sFileName, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)mt);

                FileInfo existingFile = new FileInfo(sFileName);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {

                    #region Crea File LOG
                    string sPathTXTErrors = "";
                    string s = "";
                    int ilines = 0;
                    for (ilines = sFileName.Length - 1; ilines > 0; ilines--)
                    {
                        s = sFileName.Substring(ilines, 1);
                        if (s == "\\")
                        {
                            break;
                        }
                    }
                    if (ilines == 0)
                    {
                        sVal = "Path directory non individuata.";
                        Log.Error(sVal);
                        return;
                    }
                    sPathTXTErrors = Left(sFileName, ilines);
                    DateTime saveNow = DateTime.Now;
                    long lVal = (saveNow.Year * 10000 + saveNow.Month * 100 + saveNow.Day);
                    long lVal1 = (saveNow.Hour * 10000 + saveNow.Minute * 100 + saveNow.Second);
                    sPathTXTErrors = sPathTXTErrors + "\\Log_ImportListiniXLS_" + lVal.ToString().Trim() + "_" + lVal1.ToString().Trim() + ".txt";
                    FileStream fs1 = new FileStream(sPathTXTErrors, FileMode.OpenOrCreate, FileAccess.Write);
                    writer = new StreamWriter(fs1);
                    string sRow = "";
                    writer.WriteLine("-----------------------------------------------------------------");
                    writer.WriteLine("file letto: " + sFileName);
                    writer.WriteLine("-----------------------------------------------------------------");
                    #endregion

                    int iVal = 0;
                    int lErrCode = 0, lRetCode = 0;
                    string sErrMsg = "";
                    lVal = 0;
                    double result = 0, dVal = 0;
                    string sSQL = "";
                    string sCodeBP = "", sCodeItem = "";
                    long lAlte = 0, lLarg = 0, lProg = 0;
                    
                    double dArea = 0;
                    string sArea = "";
                    bool b_TestExistTMP, bSaltaRiga = false, b_TestExist;
                    SAPbobsCOM.UserTable oUserTable = null;

                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    int colCount = worksheet.Dimension.End.Column;  //get Column Count
                    int rowCount = worksheet.Dimension.End.Row;     //get row count
                    for (int row = 2; row <= rowCount; row++) //salto i titoli i dati iniziano dalla seconda riga
                    {

                        sVal = worksheet.Cells[row, 1].Value?.ToString().Trim(); //La prima colonna deve essere sempre impostata
                        if (string.IsNullOrEmpty(sVal))
                        {
                            sVal = "";
                        }
                        if (sVal == "")
                        {
                            sVal = "- Riga " + row.ToString() + ": Terminata lettura del file.";
                            Log.Information(sVal);
                            break;
                        }

                        lVal = (int)lLetti % 100;
                        if (lVal == 0)
                        {
                            sValTMP = " Letti nr. " + lLetti.ToString().Trim();
                            sValTMP = sValTMP + ", modificati nr. " + lModificati.ToString().Trim();
                            sValTMP = sValTMP + ", aggiunti nr. " + lAggiunti.ToString().Trim();
                            sValTMP = sValTMP + ".";
                            mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                            AppInfo.Application.StatusBar.SetText(sValTMP, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)mt);
                        }

                        #region 1° colonna ---> Codice Cliente                       
                        sCodeBP = worksheet.Cells[row, 1].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(sCodeBP))
                        {
                            sCodeBP = "";
                        }
                        if (sCodeBP == "")
                        {
                            sVal = "- Riga " + row.ToString() + ": CODICE CLIENTE non impostato.";
                            Log.Error(sVal);
                            break;
                        }
                        #endregion

                        #region 2° colonna ---> Codice Articolo                        
                        sCodeItem = worksheet.Cells[row, 2].Value?.ToString().Trim();
                        if (string.IsNullOrEmpty(sCodeItem))
                        {
                            sCodeItem = "";
                        }
                        if (sCodeItem == "")
                        {
                            sVal = "- Riga " + row.ToString() + ": CODICE ARTICOLO non impostato.";
                            Log.Error(sVal);
                            break;
                        }
                        #endregion

                        bSaltaRiga = false;
                        #region verifica esistenza anagrafica cliente
                        if (sCodeBP != "")
                        {
                            b_TestExistTMP = false;
                            oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            oRecordSet.DoQuery("SELECT * FROM OCRD WHERE CardCode='" + sCodeBP + "'");
                            while (!(oRecordSet.EoF))
                            {
                                sVal = oRecordSet.Fields.Item("CardCode").Value.ToString().Trim();
                                b_TestExistTMP = true;
                                //
                                oRecordSet.MoveNext();
                            }
                            SSACommon.Disposer.ReleaseComObject(oRecordSet);
                            oRecordSet = null;
                            GC.Collect();
                            if (!b_TestExistTMP)
                            {
                                sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": CLIENTE non trovato in anagrafica.";
                                writer.WriteLine(sVal);
                                Log.Error(sVal);
                                bSaltaRiga = true;
                            }
                        }
                        #endregion

                        #region verifica esistenza anagrafica articolo
                        if (sCodeItem != "")
                        {
                            b_TestExistTMP = false;
                            oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            oRecordSet.DoQuery("SELECT * FROM OITM WHERE ItemCode='" + sCodeItem + "'");
                            while (!(oRecordSet.EoF))
                            {
                                sVal = oRecordSet.Fields.Item("ItemCode").Value.ToString().Trim();
                                b_TestExistTMP = true;
                                //
                                oRecordSet.MoveNext();
                            }
                            SSACommon.Disposer.ReleaseComObject(oRecordSet);
                            oRecordSet = null;
                            GC.Collect();
                            if (!b_TestExistTMP)
                            {
                                sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": ARTICOLO non trovato in anagrafica.";
                                writer.WriteLine(sVal);
                                Log.Error(sVal);
                                bSaltaRiga = true;
                            }
                        }
                        #endregion

                        lAlte = 0;
                        lLarg = 0;
                        dArea = 0;
                        sArea = "0";

                        #region 3° colonna ---> Altezza
                        sVal = worksheet.Cells[row, 3].Value?.ToString().Trim();
                        if (sVal != "")
                        {
                            if (!double.TryParse(sVal, out result))
                            {
                                //String                            
                            }
                            else
                            {
                                //Numeric
                                lAlte = Convert.ToInt32(sVal);
                            }
                        }
                        #endregion

                        #region 4° colonna ---> Larghezza
                        sVal = worksheet.Cells[row, 4].Value?.ToString().Trim();
                        if (sVal != "")
                        {
                            if (!double.TryParse(sVal, out result))
                            {
                                //String                            
                            }
                            else
                            {
                                //Numeric
                                lLarg = Convert.ToInt32(sVal);
                            }
                        }
                        #endregion

                        #region 5° colonna ---> Area       
                        sVal = worksheet.Cells[row, 5].Value?.ToString().Trim();
                        if (sVal != "")
                        {
                            if (!double.TryParse(sVal, out result))
                            {
                                //String                            
                            }
                            else
                            {
                                //Numeric
                                dArea = Convert.ToDouble(sVal);
                                sArea = dArea.ToString().Trim();
                                sArea = sArea.Replace(".", "");
                                sArea = sArea.Replace(",", ".");
                            }
                        }
                        #endregion

                        if (lAlte == 0 & lLarg == 0 & dArea == 0)
                        {
                            sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": ALT. e LARG. o AREA non specificata.";
                            writer.WriteLine(sVal);
                            Log.Error(sVal);
                            bSaltaRiga = true;
                        }
                        if (lAlte != 0 & lLarg == 0)
                        {
                            sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": LARG. non specificata.";
                            writer.WriteLine(sVal);
                            Log.Error(sVal);
                            bSaltaRiga = true;
                        }
                        if (lAlte == 0 & lLarg != 0)
                        {
                            sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": ALT. non specificata.";
                            writer.WriteLine(sVal);
                            Log.Error(sVal);
                            bSaltaRiga = true;
                        }

                        #region Verifica se esiste già
                        if (!bSaltaRiga)
                        {
                            sVal = "";
                            b_TestExist = false;
                            oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            sSQL = @"SELECT T0.[Code] 
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"' and  T0.[U_SSAINFO_ALTE] =" + lAlte.ToString().Trim() + " and  T0.[U_SSAINFO_LARG] =" + lLarg.ToString().Trim() + " and isnull(T0.[U_SSAINFO_AREAMQ],0) =" + sArea;
                            oRecordSet.DoQuery(sSQL);
                            while (!(oRecordSet.EoF))
                            {
                                sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                b_TestExist = true;
                                //
                                oRecordSet.MoveNext();
                            }
                            SSACommon.Disposer.ReleaseComObject(oRecordSet);
                            oRecordSet = null;
                            GC.Collect();

                            if (b_TestExist && sVal == "")
                            {
                                sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": errore nella procedura (record trovato ma non identificato test1).";
                                writer.WriteLine(sVal);
                                Log.Error(sVal);
                                bSaltaRiga = true;
                            }
                        }
                        #endregion

                        b_TestExist = false;
                        if (!bSaltaRiga)
                        {
                            //CompanyInfo.Company.StartTransaction();
                            oUserTable = CompanyInfo.Company.UserTables.Item("SSAINFO_LISALTLAR");
                            if (sVal != "")
                            {
                                b_TestExist = oUserTable.GetByKey(sVal);
                                if (!b_TestExist)
                                {
                                    sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": errore nella procedura (record trovato ma non identificato test2).";
                                    writer.WriteLine(sVal);
                                    Log.Error(sVal);
                                    bSaltaRiga = true;
                                }
                            }
                        }

                        if (!bSaltaRiga)
                        {
                            #region se non esiste già lo creo
                            if (!b_TestExist)
                            {
                                //Cerco il nuovo progressivo
                                lProg = 1;
                                oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                sSQL = @"SELECT T0.[Code]
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
ORDER BY CAST(T0.[Code] AS BIGINT) DESC";
                                oRecordSet.DoQuery(sSQL);
                                while (!(oRecordSet.EoF))
                                {
                                    sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                    lProg = Convert.ToInt64(sVal);
                                    lProg++;
                                    //
                                    break;
                                    //oRecordSet.MoveNext();
                                }
                                oRecordSet = null;
                                GC.Collect();
                                //

                                oUserTable.Code = lProg.ToString().Trim();
                                oUserTable.Name = lProg.ToString().Trim();
                                oUserTable.UserFields.Fields.Item("U_SSAINFO_CODCLI").Value = sCodeBP;
                                oUserTable.UserFields.Fields.Item("U_SSAINFO_CODART").Value = sCodeItem;
                                oUserTable.UserFields.Fields.Item("U_SSAINFO_ALTE").Value = Convert.ToDouble(lAlte);
                                oUserTable.UserFields.Fields.Item("U_SSAINFO_LARG").Value = Convert.ToDouble(lLarg);
                                oUserTable.UserFields.Fields.Item("U_SSAINFO_AREAMQ").Value = dArea;
                            }
                            #endregion

                            //6° colonna 
                            dVal = 0;
                            sVal = worksheet.Cells[row, 6].Value?.ToString().Trim();
                            if (sVal != "")
                            {
                                if (!double.TryParse(sVal, out result))
                                {
                                    //String                            
                                }
                                else
                                {
                                    //Numeric
                                    dVal = Convert.ToDouble(sVal);
                                }
                            }
                            oUserTable.UserFields.Fields.Item("U_SSAINFO_LIST").Value = dVal;

                            //dalla 7° colonna COD. MAT 01 e COEFF. MAT. 01 ... 14
                            int iField = 6;
                            string sfield = "", sCodMat = "";
                            double dCoefMat = 0;
                            for (int i = 1; i <= 14; i++)
                            {
                                //-"Codice Mat. ..."
                                iField = iField + 1;
                                sfield = "U_SSAINFO_MAID" + Right("0" + i.ToString().Trim(), 2);
                                sCodMat = worksheet.Cells[row, iField].Value?.ToString().Trim();
                                if (string.IsNullOrEmpty(sCodMat))
                                {
                                    sCodMat = "";
                                }
                                #region verifica esistenza anagrafica articolo materiale
                                if (sCodMat != "")
                                {
                                    b_TestExistTMP = false;
                                    oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                    oRecordSet.DoQuery("SELECT * FROM OITM WHERE ItemCode='" + sCodMat + "'");
                                    while (!(oRecordSet.EoF))
                                    {
                                        sVal = oRecordSet.Fields.Item("ItemCode").ToString().Trim();
                                        b_TestExistTMP = true;
                                        //
                                        oRecordSet.MoveNext();
                                    }
                                    oRecordSet = null;
                                    GC.Collect();
                                    if (!b_TestExistTMP)
                                    {
                                        sVal = "- Riga " + row.ToString() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": MATERIALE " + sCodMat + " non trovato in anagrafica.";
                                        writer.WriteLine(sVal);
                                        Log.Error(sVal);
                                        return;
                                    }
                                }

                                #endregion
                                oUserTable.UserFields.Fields.Item(sfield).Value = sCodMat;
                                //-creare campo "Coeff. Mat. ..."
                                iField = iField + 1;
                                sfield = "U_SSAINFO_MAQU" + Right("0" + i.ToString().Trim(), 2);
                                dCoefMat = 0;
                                sVal = worksheet.Cells[row, iField].Value?.ToString().Trim();
                                if (sVal != "")
                                {
                                    if (!double.TryParse(sVal, out result))
                                    {
                                        //String                            
                                    }
                                    else
                                    {
                                        //Numeric
                                        dCoefMat = Convert.ToDouble(sVal);
                                    }
                                }
                                oUserTable.UserFields.Fields.Item(sfield).Value = dCoefMat;
                                //
                            }
                            //
                            if (!b_TestExist)
                            {
                                lAggiunti++;
                                lRetCode = oUserTable.Add();
                            }
                            else
                            {
                                lModificati++;
                                lRetCode = oUserTable.Update();
                            }
                            if (lRetCode != 0)
                            {
                                CompanyInfo.Company.GetLastError(out lErrCode, out sErrMsg);
                                sRow = "- riga " + (lLetti + 1).ToString().Trim() + " Cl. " + sCodeBP + " Art. " + sCodeItem + ": " + sErrMsg + " (" + lErrCode + ").";
                                writer.WriteLine(sRow);
                                //return sRow;
                            }
                            /*
                            if (CommonCsSSA.oCompany.InTransaction)
                            {
                                //oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                                CommonCsSSA.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                            }
                            else
                            {
                                throw new Exception("ERROR: Transaction closed before EndTransaction");
                            }*/                            
                        }
                        lLetti++;
                    }
                }
                mt = SAPbouiCOM.BoStatusBarMessageType.smt_Success;
                sValTMP = " Letti nr. " + lLetti.ToString().Trim();
                sValTMP = sValTMP + ", modificati nr. " + lModificati.ToString().Trim();
                sValTMP = sValTMP + ", aggiunti nr. " + lAggiunti.ToString().Trim();
                sValTMP = sValTMP + ".";
                AppInfo.Application.StatusBar.SetText("Terminato import listini dal file " + sFileName+". "+ sValTMP, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)mt);

            }
            catch (Exception ex)
            {
                mt = SAPbouiCOM.BoStatusBarMessageType.smt_Error;
                AppInfo.Application.StatusBar.SetText("Terminato import listini dal file " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)mt);
                Log.Error($"ERROR ImportDatiXLS_SSAINFO_LISALTLAR: {ex.Message}");
            }

        }

        public static string ReadItemCodePrice(string sItemCode, string sCardCode, ref double dUnitPrice)
        {
            try
            {
                SAPbobsCOM.CompanyService oCompanyService = null;
                oCompanyService = CompanyInfo.Company.GetCompanyService();
                SAPbobsCOM.ItemPriceParams param = oCompanyService.GetDataInterface(SAPbobsCOM.CompanyServiceDataInterfaces.csdiItemPriceParams) as SAPbobsCOM.ItemPriceParams;
                param.FromXMLString(@"<?xml version='1.0' encoding='UTF-16'?>
                        <ItemPriceParams>
                            <Date>" + DateTime.Now.Date.ToString("yyyyMMdd") + @"</Date>
                            <UoMEntry nil='true'></UoMEntry>
                            <BlanketAgreementNumber nil='true'></BlanketAgreementNumber>
                            <BlanketAgreementLine nil='true'></BlanketAgreementLine>
                            <UoMQuantity nil='true'></UoMQuantity>
                            <InventoryQuantity>1.000000</InventoryQuantity>
                            <Currency nil='true'></Currency>
                            <ItemCode>" + sItemCode + @"</ItemCode>
                            <CardCode>" + sCardCode + @"</CardCode>
                            <PriceList nil='true'></PriceList>
                        </ItemPriceParams>");
                SAPbobsCOM.ItemPriceReturnParams rtnParams = oCompanyService.GetItemPrice(param);
                dUnitPrice = rtnParams.Price;
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rtnParams);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(param);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oCompanyService);

                return "";
            }
            catch (Exception ex)
            {
                return (ex.Message);
            }
            finally
            {
            }
        }

    }
}
