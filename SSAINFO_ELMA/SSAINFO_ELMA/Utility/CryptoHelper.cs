﻿#region system
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

#endregion

namespace SSAINFO_ELMA.Utility
{
    /// <summary>
    /// The cryptography helper
    /// </summary>
    public static class CryptoHelper
    {

        #region private constants

        private static readonly byte[] ED_KEY = new byte[] { 77, 26, 97, 200, 15, 143, 170, 210, 53, 88, 34, 99, 111, 161, 219, 42 };
        private static readonly byte[] ED_IV = new byte[] { 125, 67, 50, 231, 79, 43, 176, 130 };

        #endregion

        #region public methods

        /// <summary>
        /// Encodes the specified plain string.
        /// </summary>
        /// <param name="plainStr">The plain string.</param>
        /// <returns></returns>
        public static string Encode(string plainStr)
        {
            string result;

            result = Encrypt(plainStr);
            return result;
        }


        /// <summary>
        /// Decodes the specified encoded string.
        /// </summary>
        /// <param name="encodedStr">The encoded string.</param>
        /// <returns></returns>
        public static string Decode(string encodedStr)
        {
            string result;

            result = Decrypt(encodedStr);
            return result;
        }

        /// <summary>
        /// Generate an hash from a plain string.
        /// </summary>
        /// <param name="plainStr">The plain string.</param>
        /// <returns></returns>
        public static string HashEncode(string plainStr)
        {
            string result;

            result = CalculateMD5Hash(plainStr);
            return result;
        }

        #endregion

        #region private methods

        private static string Encrypt(string message)
        {
            ASCIIEncoding textConverter;
            RC2CryptoServiceProvider rc2CSP;
            ICryptoTransform encryptor;
            MemoryStream msEncrypt;
            CryptoStream csEncrypt;
            string encryptedMessage;
            byte[] encrypted;
            byte[] toEncrypt;
            byte[] key;
            byte[] IV;

            //Initialization
            rc2CSP = new RC2CryptoServiceProvider();
            rc2CSP.Key = ED_KEY;
            rc2CSP.IV = ED_IV;

            //Get the key and IV.
            key = rc2CSP.Key;
            IV = rc2CSP.IV;

            //Get an encryptor.
            encryptor = rc2CSP.CreateEncryptor(key, IV);

            //Encrypt the data.
            msEncrypt = new MemoryStream();
            csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);

            //Convert the data to a byte array.
            textConverter = new ASCIIEncoding();
            toEncrypt = textConverter.GetBytes(message);

            //Write all data to the crypto stream and flush it.
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            //Get encrypted array of bytes.
            encrypted = msEncrypt.ToArray();
            encryptedMessage = Convert.ToBase64String(encrypted);

            return encryptedMessage;
        }

        private static string Decrypt(string cryptedMessage)
        {
            ASCIIEncoding textConverter;
            RC2CryptoServiceProvider rc2CSP;
            ICryptoTransform decryptor;
            MemoryStream msDecrypt;
            CryptoStream csDecrypt;
            string message;
            char emptyChr;
            byte[] key;
            byte[] IV;
            byte[] encrypted;
            byte[] fromEncrypt;

            //Initialization
            rc2CSP = new RC2CryptoServiceProvider();
            rc2CSP.Key = ED_KEY;
            rc2CSP.IV = ED_IV;

            //Get the key and IV.
            key = rc2CSP.Key;
            IV = rc2CSP.IV;

            //Get encrypted array of bytes.
            encrypted = Convert.FromBase64String(cryptedMessage);
            decryptor = rc2CSP.CreateDecryptor(key, IV);

            //Decrypt the data.
            msDecrypt = new MemoryStream(encrypted);
            csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);

            //Read the data out of the crypto stream.
            fromEncrypt = new byte[encrypted.Length];
            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

            //Remove null charaters, may create problems with serialization
            emptyChr = default(char);
            var tmp = from b in fromEncrypt
                      where b != emptyChr
                      select b;
            fromEncrypt = Enumerable.ToArray(tmp);

            //Convert the byte array back into a string.
            textConverter = new ASCIIEncoding();
            message = textConverter.GetString(fromEncrypt);

            return message;
        }

        private static string CalculateMD5Hash(string input)
        {
            MD5 md5;
            StringBuilder sb;
            byte[] inputBytes;
            byte[] hash;

            //Step 1, calculate MD5 hash from input.
            md5 = MD5.Create();
            inputBytes = Encoding.ASCII.GetBytes(input);
            hash = md5.ComputeHash(inputBytes);

            //Step 2, convert byte array to hex string.
            sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                //It formats the string as two uppercase hexadecimal characters.
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        #endregion

    }
}
