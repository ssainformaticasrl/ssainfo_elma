﻿using SAPbouiCOM.Framework;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SSAINFO_ELMA
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    //If you want to use an add-on identifier for the development license, you can specify an add-on identifier string as the second parameter.
                    //oApp = new Application(args[0], "XXXXX");
                    oApp = new Application(args[0]);
                }
                List<SSACommon.AddonInfo> AddonList = new List<SSACommon.AddonInfo>();

                CompanyInfo.Company = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
                AppInfo.Application = Application.SBO_Application;
                SSACommon.CompanyInfo.Company = CompanyInfo.Company;
                SSACommon.DBCommon.CreaDB();
                //menu comune a tutti gli add-on eappSAP
                //SSACommon.MenuInfo.AddMenu();

                oApp.RegisterMenuEventHandler(SSACommon.MenuInfo.SBO_Application_MenuEvent);

                // inizializzazione log (with serilog)
                Log.Logger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();

                Log.Information(new string('*', 80));
                Log.Information($"Add-on SSAINFO_ELMA collegato all'azienda " + SSACommon.CompanyInfo.Company.CompanyName);
                Log.Information(new string('*', 80));
                Log.Information(string.Format("SLDServer: {0}, Server: {1}, LicenseServer: {2}, CompanyDB: {3}, B1User: {4}"
                    , CompanyInfo.Company.SLDServer
                    , CompanyInfo.Company.Server
                    , CompanyInfo.Company.LicenseServer
                    , CompanyInfo.Company.CompanyDB
                    , AppInfo.Application.Company.UserName));


                //MyMenu.AddMenuItems();

                //inserire aggancio ad altri eventuali add-on eappSAP
                AddonList.Add(new SSACommon.AddonInfo
                {
                    StartOrder = 1,
                    Code = "SSAINFO_ELMA",
                    Name = "ADDON SSAINFO_ELMA",
                    InstalledVersion = "",
                    InstalledDBVersion = AddonInfo.AddonDBVersion.ToString(),
                    AssemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    InfoFile = "info.SSAINFO_ELMA.html"
                });

                CreateDB();
                MenuInfo.AddMenu();
                MenuInfo.AddHelpMenu();
                oApp.RegisterMenuEventHandler(MenuInfo.SBO_Application_MenuEvent);
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                Application.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
                Application.SBO_Application.StatusBar.SetText("Add-on [" + AddonInfo.AddonProvider + "] " + AddonInfo.AddonName + " inizializzato e collegato alla company " + AdminInfo.CompanyName() + "(" + SSACommon.AppInfo.Application.Company.DatabaseName + ")", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                oApp.Run();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                Utility.Tools.LogError(ex, $"Errore inizializzazione ed avvio add-on.");
            }
        }

        private static void CreateDB()
        {
            try
            {
                SSACommon.AppInfo.Application.StatusBar.SetText("SSAINFO_ELMA : controllo DB....",
                     SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

                ///Esempio Scrittura tabelle e campo con nuova modalità "withxmlmethod", in cui è necessario creare un nuovo xml per ogni userfilds/table/udo

                //SSACommon.MetaData.UserTables.CreateDBTable("SSAINFO_ELMA_OMOD", "EMX: Output per Modula", SAPbobsCOM.BoUTBTableType.bott_Document, true);
                //SSACommon.MetaData.UserFields.CreateDBField("@SSAINFO_ELMA_OMOD", "SEND_DATE", "EMX: Data invio", SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 8, null, true);
                //SSACommon.MetaData.UDOmanager.UDOComparer(typeof(Program), "SSAINFO_ELMA_TEST");
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore inizializzazione campi utente e tabelle {ex.Message}.");
            }
        }

        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    //Exit Add-On
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Funzione per intercettare l'apertura delle FORM\Items di SAP
        /// </summary>
        /// <param name="FormUID"></param>
        /// <param name="pVal"></param>
        /// <param name="BubbleEvent"></param>
        private static void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction)
                {
                    switch (pVal.EventType)
                    {
                        case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK:
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    switch (pVal.EventType)
                    {
                        case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                            {
                                switch (pVal.FormTypeEx)
                                {
                                    //case "134":                               // Business partner
                                    //    break;
                                    case "139":                                 // Ordine cliente ORDR
                                        {
                                            new Form.B1Form_ORDR(pVal.FormUID);
                                        }
                                        break;
                                    case "142":                                 // Ordine acquisto OPOR
                                        {
                                            new Form.B1Form_OPOR(pVal.FormUID);
                                        }
                                        break;                                    
                                    case "149":                                 // Ordine acquisto OPOR
                                        {
                                            new Form.B1Form_OQUT(pVal.FormUID);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            }
                        default:
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Utility.Tools.LogError(ex, $"Errore avvio e controllo form, messaggio: {ex.Message}.");
            }
        }
    }
}
