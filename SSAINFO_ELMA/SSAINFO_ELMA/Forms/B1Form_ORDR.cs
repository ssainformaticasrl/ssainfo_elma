﻿using SAPbouiCOM;
using Serilog;
using SSACommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSAINFO_ELMA.Utility;
using System.Text.RegularExpressions;
using System.Globalization;

namespace SSAINFO_ELMA.Form
{
    /// <summary>
    /// Classe d'esempio per gestire alcuni controlli\eventi SAP tramite UI (lasciati commentati)
    /// </summary>
    class B1Form_ORDR : Base.Form.SBOSystemFormBase
    {
        //private string _globalCommValue;
        //private string _globalMatrValue;
        //private string _pointedLineComValue;
        private SAPbouiCOM.Item _btnExport;
        private string _uIdBtn = "btn_ExpDat";
        private SAPbouiCOM.Button _oBtn;
        private SAPbouiCOM.BoStatusBarMessageType _mt;
        //private SAPbouiCOM.EditText _editTextComm;
        //private SAPbouiCOM.EditText _editTextMatr;
        private SAPbouiCOM.Matrix _matrixLines;
        //private SAPbouiCOM.Form _myFormUser; //Form dei campi utente di testata
        public string sItemCodeGLOB = "";
        public string sLargGLOB = "";
        public string sAlteGLOB = "";
        public string sQuanGLOB = "";

        public B1Form_ORDR(string formUID)
            : base(formUID)
        {
            InitForm();
        }

        protected override void Dispose()
        {
        }

        protected override void FormDataEvent(ref BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent)
        {
            ExtractDataFromUi();
            if (BusinessObjectInfo.EventType == BoEventTypes.et_FORM_DATA_LOAD && BusinessObjectInfo.BeforeAction == false && BusinessObjectInfo.ActionSuccess == true)
            {
                string docEntry = MyDBDataSources("ORDR").GetValue("DocEntry", 0);
            }
        }

        private void ExtractDataFromUi()
        {
            try
            {
                //AppInfo.Application.ActivateMenuItem("6913");                   // apre area campi utente (menu)
                //_editTextComm = ((SAPbouiCOM.EditText)MyUDFForm.Items.Item("U_SSAINFO_COM").Specific);
                _matrixLines = (SAPbouiCOM.Matrix)MyForm.Items.Item("38").Specific;
                //_globalCommValue = _editTextComm.Value;
            }
            catch (Exception ex)
            {
            }
        }

        protected override void InitForm()
        {
            try
            {
                _btnExport = MyForm.Items.Add(_uIdBtn, SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                _oBtn = (SAPbouiCOM.Button)_btnExport.Specific;
                _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                _oBtn.PressedAfter += new SAPbouiCOM._IButtonEvents_PressedAfterEventHandler(this.btnExport_PressedAfter);
                ExtractDataFromUi();
            }
            catch (Exception ex)
            {
                Log.Error($"Errore nella creazione di un pulsante aggiuntivo via UI API, messaggio: {ex.Message}");
            }
        }

        private void btnExport_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                string sVal = "";

                if (pVal.FormMode != Convert.ToInt32(SAPbouiCOM.BoFormMode.fm_OK_MODE) & pVal.FormMode != Convert.ToInt32(SAPbouiCOM.BoFormMode.fm_UPDATE_MODE))
                {
                    sVal = "Attivazione prevista solo da documenti già salvati!";
                    AppInfo.Application.StatusBar.SetText(sVal, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    AppInfo.Application.MessageBox(sVal, 1, "Ok", "", "");
                    return;
                }
                string sCodeBP = "", sArtNo = "", sCode = "", sfield = "", sCodMat = "", sMagDef = "", sItemName = "";
                double dVal = 0, dVal1 = 0, dAlte = 0, dAltePrec = 0, dLarg = 0, dLargPrec = 0, dCoefMat = 0, dQtaOP = 0, dPrUn = 0;
                double dScosAlte = 0, dScosLarg = 0;
                int iAlteUM = 0, iLargUM = 0;
                SAPbouiCOM.EditText oEdtxt = null;
                oEdtxt = (SAPbouiCOM.EditText)MyForm.Items.Item("8").Specific;
                string sDocEntryOP, sDocNumOP, sDocEntryOA, sDocNumOA, sFoCo = "", sDocNum = oEdtxt.Value;
                int iDocEntry = int.Parse(MyForm.DataSources.DBDataSources.Item("ORDR").GetValue("DocEntry", 0).Trim());
                int nMatAddedOA = 0;
                int iCountMat = 0, iVal = 0, l = 0; ;
                int lRetCode = 0, lErrCode = 0;
                string sErrMsg = "";
                SAPbobsCOM.Recordset oRecordSet;
                SAPbobsCOM.Recordset oRecordSet1;
                SAPbobsCOM.Recordset oRecordSet2;
                SAPbobsCOM.Documents oPor;
                bool b_TestExist = false, bTestUpdateDoc = false;
                SAPbobsCOM.ProductionOrders productionOrder = null;
                string sMsgMain = "", sValTmp = "";
                oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string sSQL = @"SELECT T.[DocNum] OC_DocNum,T.[CardCode] OC_CardCode, T.[DocEntry] OC_DocEntry,R.ItemCode OC_ItemCode,T_OP.[DocNum] OP_DocNum, T_OP.[DocEntry] OP_DocEntry , T_OP.[ItemCode] OP_ItemCode
,T_OP.PlannedQty,R.Height1,R.Width1,r.Hght1Unit,r.Wdth1Unit
FROM ORDR T 
INNER JOIN RDR1 R ON T.[DocEntry] = R.[DocEntry]
INNER JOIN OWOR T_OP ON T_OP.LinkToObj = T.ObjType AND T_OP.OriginAbs = T.DocEntry AND T_OP.Project = R.Project AND T_OP.[Status] <>'L' AND  T_OP.[Status] <>'C'
WHERE T.[DocEntry]=" + iDocEntry.ToString();
                //Escludi ordini di produzione chiusi (L) e interrotti (C)
                oRecordSet.DoQuery(sSQL);
                if (oRecordSet.RecordCount == 0)
                {
                    AppInfo.Application.StatusBar.SetText("Non sono stati trovati ordini di produzione non chiusi o interrotti collegatti all'ordine cliente " + sDocNum, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                    return;
                }
                // richiesta operazione
                if (AppInfo.Application.MessageBox($"Sono stati trovani n {oRecordSet.RecordCount} ordini di produzione, procedere con l'operazione di propagazione? ", 2, "Sì", "No") == 1)
                {
                    sValTmp = $"Trovati nr {oRecordSet.RecordCount} OP.";
                    sMsgMain = sMsgMain + sValTmp + "\r\n";
                    AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None);

                    while (!(oRecordSet.EoF))
                    {
                        //per ogni OP
                        sCodeBP = oRecordSet.Fields.Item("OC_CardCode").Value.ToString().Trim();
                        sArtNo = oRecordSet.Fields.Item("OP_ItemCode").Value.ToString().Trim();
                        sDocEntryOP = oRecordSet.Fields.Item("OP_DocEntry").Value.ToString().Trim();
                        sDocNumOP = oRecordSet.Fields.Item("OP_DocNum").Value.ToString().Trim();
                        dQtaOP = Convert.ToDouble(oRecordSet.Fields.Item("PlannedQty").Value);
                        dAlte = Convert.ToDouble(oRecordSet.Fields.Item("Height1").Value);
                        iAlteUM = Convert.ToInt32(oRecordSet.Fields.Item("Hght1Unit").Value);
                        dLarg = Convert.ToDouble(oRecordSet.Fields.Item("Width1").Value);
                        iLargUM = Convert.ToInt32(oRecordSet.Fields.Item("Wdth1Unit").Value);

                        #region 1.1. ricerca elemento in @SSAINFO_LISALTLAR
                        oRecordSet1 = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_LIST],isnull(T0.[U_SSAINFO_ALTE],0) as U_SSAINFO_ALTE, isnull(T0.[U_SSAINFO_LARG],0) as U_SSAINFO_LARG
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sArtNo + @"'
ORDER BY T0.[U_SSAINFO_ALTE] DESC, T0.[U_SSAINFO_LARG] DESC";
                        oRecordSet1.DoQuery(sSQL);
                        sCode = "";
                        if (oRecordSet1.EoF)
                        {
                            sValTmp = $"- OP {sDocNumOP} non trovato articolo-cliente in @SSAINFO_LISALTLAR.";
                            sMsgMain = sMsgMain + sValTmp + "\r\n";
                            AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                        }
                        while (!(oRecordSet1.EoF))
                        {
                            sVal = oRecordSet1.Fields.Item("Code").Value.ToString().Trim();
                            //Controllo sull'altezza
                            dVal = Convert.ToDouble(oRecordSet1.Fields.Item("U_SSAINFO_ALTE").Value);
                            if (dVal < dAlte)
                            {
                                break;
                            }
                            if (dVal != dAltePrec)
                            {
                                sCode = "";
                            }
                            //Controllo sulla larghezza     
                            dVal1 = Convert.ToDouble(oRecordSet1.Fields.Item("U_SSAINFO_LARG").Value);
                            if (dVal == 1950 & dVal1 == 250)
                            {
                                sVal = "";
                            }
                            //se ci si trova alla stessa altezza con larghezza superiore vuol dire che non devo prendere il considerazione il range corrente
                            //perchè successivo
                            //se ci si trova alla stessa altezza con larghezza inferiore vuol dire che non devo prendere il considerazione il range corrente
                            //if ((dVal == dAltePrec) & (dVal1 > dLarg))
                            if ((dVal == dAltePrec) & (dVal1 < dLarg))
                            {

                            }
                            else
                            {
                                //if ((dVal != dAltePrec) & (dVal1 > dLarg)) //se è cambiata l'altezza e la larghezza è inferiore allora NON esiste il range
                                if ((dVal != dAltePrec) & (dVal1 < dLarg)) //se è cambiata l'altezza e la larghezza è superiore allora NON esiste il range                                  
                                {

                                }
                                else
                                {
                                    sCode = oRecordSet1.Fields.Item("Code").Value.ToString().Trim();
                                }
                            }
                            dAltePrec = dVal;
                            dLargPrec = dVal1;
                            //
                            oRecordSet1.MoveNext();
                        }
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet1);
                        oRecordSet1 = null;
                        #endregion

                        #region 1.2. Se è stato trovato il corrispondente elemento tabella @SSAINFO_LISALTLAR aggiunge in OP i materiali mancanti
                        if (sCode == "")
                        {
                            sValTmp = $"- OP {sDocNumOP} non trovato range corrispondente in @SSAINFO_LISALTLAR.";
                            sMsgMain = sMsgMain + sValTmp + "\r\n";
                            AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                        }
                        if (sCode != "")
                        {
                            oRecordSet1 = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                            sSQL = @"SELECT T0.*
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[Code] ='" + sCode + @"'";
                            oRecordSet1.DoQuery(sSQL);
                            while (!(oRecordSet1.EoF)) //Ne trova solo 1
                            {
                                //Cerco OP
                                productionOrder = (SAPbobsCOM.ProductionOrders)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                                iVal = Convert.ToInt32(sDocEntryOP);
                                b_TestExist = productionOrder.GetByKey(iVal);
                                if (!b_TestExist)
                                {
                                    sValTmp = $"- OP {sDocNumOP} non è stato trovato l'OP.";
                                    sMsgMain = sMsgMain + sValTmp + "\r\n";
                                    AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                    return;
                                }
                                iCountMat = 0;
                                bTestUpdateDoc = false;
                                for (int i = 1; i <= 14; i++)
                                {
                                    //-"Codice Mat. ..."                            
                                    sfield = "U_SSAINFO_MAID" + Tools.Right("0" + i.ToString().Trim(), 2);
                                    sCodMat = oRecordSet1.Fields.Item(sfield).Value.ToString().Trim();
                                    if (sCodMat != "")
                                    {
                                        //Leggiamo coefficiente da tabella predefinita
                                        sfield = "U_SSAINFO_MAQU" + Tools.Right("0" + i.ToString().Trim(), 2);
                                        dCoefMat = Convert.ToDouble(oRecordSet1.Fields.Item(sfield).Value);
                                        #region verifica esistenza anagrafica articolo materiale
                                        sVal = "";
                                        sMagDef = "";
                                        sItemName = "";
                                        oRecordSet2 = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        oRecordSet2.DoQuery("SELECT * FROM OITM WHERE ItemCode='" + sCodMat + "'");
                                        while (!(oRecordSet2.EoF))
                                        {
                                            sVal = oRecordSet2.Fields.Item("ItemCode").Value.ToString().Trim();
                                            sMagDef = oRecordSet2.Fields.Item("DfltWH").Value.ToString().Trim();
                                            sItemName = oRecordSet2.Fields.Item("ItemName").Value.ToString().Trim();
                                            //
                                            oRecordSet2.MoveNext();
                                        }
                                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet2);
                                        oRecordSet2 = null;
                                        #endregion
                                        if (sVal == "" || sMagDef == "")
                                        {
                                            if (sVal == "")
                                            {
                                                sValTmp = $"- OP {sDocNumOP}: il materiale '{sCodMat}' non è stato trovato in anagrafica articolo e verrà ignorato.";
                                                sMsgMain = sMsgMain + sValTmp + "\r\n";
                                                AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                                            }
                                            else
                                            {
                                                if (sMagDef == "")
                                                {
                                                    sValTmp = $"- OP {sDocNumOP}: il materiale '{sCodMat}' non è stato indicato il magazzino di default.";
                                                    sMsgMain = sMsgMain + sValTmp + "\r\n";
                                                    AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            sVal = "";
                                            for (l = 0; l < productionOrder.Lines.Count; l++)
                                            {
                                                productionOrder.Lines.SetCurrentLine(l);

                                                if (productionOrder.Lines.ItemType == SAPbobsCOM.ProductionItemType.pit_Item && sCodMat == productionOrder.Lines.ItemNo)
                                                {
                                                    sVal = "*"; //Trovato!!!
                                                    break;
                                                }
                                            }
                                            if (sVal == "") //Se non è stato trovato
                                            {
                                                //Aggiungiamo il nuovo materiale in OP
                                                productionOrder.Lines.Add();
                                                l = productionOrder.Lines.Count - 1;
                                                productionOrder.Lines.SetCurrentLine(l);
                                                productionOrder.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Item;
                                                productionOrder.Lines.ItemNo = sCodMat;
                                                productionOrder.Lines.ItemName = sItemName;
                                                if (dCoefMat == 0)
                                                {
                                                    dCoefMat = 1;
                                                }
                                                productionOrder.Lines.BaseQuantity = dCoefMat;
                                                productionOrder.Lines.PlannedQuantity = dCoefMat * dQtaOP;
                                                productionOrder.Lines.Warehouse = sMagDef;
                                                iCountMat++;
                                                bTestUpdateDoc = true;
                                            }
                                        }
                                    }
                                    //
                                }
                                if (bTestUpdateDoc)
                                {
                                    lRetCode = productionOrder.Update();
                                    if (lRetCode != 0)
                                    {
                                        CompanyInfo.Company.GetLastError(out lErrCode, out sErrMsg);
                                        sValTmp = $"- OP {sDocNumOP}: tentativo di aggiunta di " + " nr " + iCountMat.ToString().Trim() + " materiali: " + sErrMsg + " (" + lErrCode + ").";
                                        sMsgMain = sMsgMain + sValTmp + "\r\n";
                                        AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                        return;
                                    }
                                    else
                                    {
                                        sValTmp = $"- OP {sDocNumOP}: aggiunta di " + " nr " + iCountMat.ToString().Trim() + " materiali.";
                                        sMsgMain = sMsgMain + sValTmp + "\r\n";
                                        AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                    }
                                }
                                //
                                oRecordSet1.MoveNext();
                            }
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet1);
                            oRecordSet1 = null;
                            GC.Collect();
                        }
                        #endregion

                        #region 2. cerca tutti gli ordini d'acquisto collegati all'ordine di produzione
                        oRecordSet1 = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        sSQL = @"SELECT T_OP.[DocNum] OP_DocNum, T_OP.[DocEntry] OP_DocEntry , T_OP.[ItemCode] OP_ItemCode,T_OA.[DocEntry] OA_DocEntry,T_OA.[DocNum] OA_DocNum,T_OA.[CardCode] OA_CardCode--, R_OA.[ItemCode]
,R.Height1,R.Width1,r.Hght1Unit,r.Wdth1Unit
FROM OWOR T_OP
JOIN WOR1 R_OP ON R_OP.DocEntry=T_OP.DocEntry
JOIN OPOR T_OA ON T_OA.DocEntry=R_OP.PoDocEntry AND T_OA.ObjType=R_OP.PoDocType
JOIN RDR1 R ON T_OP.LinkToObj = 17 AND T_OP.OriginAbs = R.DocEntry AND T_OP.Project = R.Project
--JOIN POR1 R_OA ON R_OA.DocEntry=R_OP.PoDocEntry AND R_OA.LineNum=R_OP.PoLineNum
WHERE T_OP.[DocEntry]=" + sDocEntryOP + @"
group by T_OP.[DocNum], T_OP.[DocEntry] , T_OP.[ItemCode],T_OA.[DocEntry] ,T_OA.[DocNum],T_OA.[CardCode]
,R.Height1,R.Width1,r.Hght1Unit,r.Wdth1Unit
order by T_OP.[DocNum], T_OP.[DocEntry] , T_OP.[ItemCode],T_OA.[DocEntry],T_OA.[DocNum],T_OA.[CardCode]
,R.Height1,R.Width1,r.Hght1Unit,r.Wdth1Unit";
                        oRecordSet1.DoQuery(sSQL);
                        sCode = "";
                        while (!(oRecordSet1.EoF))
                        {
                            sDocNumOA = oRecordSet1.Fields.Item("OA_DocNum").Value.ToString().Trim();
                            sDocEntryOA = oRecordSet1.Fields.Item("OA_DocEntry").Value.ToString().Trim();

                            oPor = (SAPbobsCOM.Documents)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);
                            b_TestExist = oPor.GetByKey(Convert.ToInt32(sDocEntryOA));
                            if (!(b_TestExist))
                            {
                                sValTmp = $"- OP {sDocNumOP} OA {sDocNumOA}: non è stato trovato OA (" + sDocEntryOA + ").";
                                sMsgMain = sMsgMain + sValTmp + "\r\n";
                                AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                return;
                            }
                            else
                            {
                                //if (oPor.NumAtCard != sDocNum)
                                //{
                                oPor.NumAtCard = sDocNum;
                                sFoCo = oRecordSet1.Fields.Item("OA_CardCode").Value.ToString().Trim();

                                for (int i = 0; i < oPor.Lines.Count; i++)
                                {
                                    oPor.Lines.SetCurrentLine(i);

                                    if (oPor.Lines.ItemCode != "" && oPor.Lines.BaseEntry == Convert.ToInt32(sDocEntryOP))
                                    {
                                        sCodMat = oPor.Lines.ItemCode;
                                        dAlte = Convert.ToDouble(oRecordSet.Fields.Item("Height1").Value);
                                        iAlteUM = Convert.ToInt32(oRecordSet.Fields.Item("Hght1Unit").Value);
                                        dLarg = Convert.ToDouble(oRecordSet.Fields.Item("Width1").Value);
                                        iLargUM = Convert.ToInt32(oRecordSet.Fields.Item("Wdth1Unit").Value);
                                        //Cerco scostamento
                                        dScosAlte = 0;
                                        dScosLarg = 0;
                                        sSQL = @"SELECT T1.[U_SSAINFO_SCOSALTE], T1.[U_SSAINFO_SCOSLARG]
FROM OITM T1 
WHERE T1.[ItemCode]='" + sCodMat + "' and T1.[U_SSAINFO_MTOACQFOCO]='" + sFoCo + @"'";
                                        oRecordSet2 = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        oRecordSet2.DoQuery(sSQL);
                                        while (!(oRecordSet2.EoF))
                                        {
                                            dScosAlte = Convert.ToDouble(oRecordSet2.Fields.Item("U_SSAINFO_SCOSALTE").Value);
                                            dScosLarg = Convert.ToDouble(oRecordSet2.Fields.Item("U_SSAINFO_SCOSLARG").Value);
                                            //
                                            oRecordSet2.MoveNext();
                                        }
                                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet2);
                                        oRecordSet2 = null;
                                        dAlte = dAlte - dScosAlte;
                                        oPor.Lines.Height1 = dAlte;
                                        oPor.Lines.Hight1Unit = iAlteUM;
                                        dLarg = dLarg - dScosLarg;
                                        oPor.Lines.Width1 = dLarg;
                                        oPor.Lines.Width1Unit = iLargUM;
                                        /*dPrUn = 0;
                                        sVal = Tools.ReadItemCodePrice(sCodMat, sFoCo, ref dPrUn);
                                        if (sVal == "")
                                        {
                                            AppInfo.Application.StatusBar.SetText("Errore nel reperimento del prezzo per il materiale '" + sCodMat + "' dell'ordine d'acquisto nr " + sDocNumOA + " (" + sDocEntryOA + ") rif OP nr " + sDocNumOP + " (" + sDocEntryOP + ")", SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                            return;
                                        }*/
                                        dPrUn = oPor.Lines.UnitPrice;
                                        dVal = dAlte * dLarg * dPrUn / 1000000;
                                        dVal = Math.Round(dVal, 2);
                                        oPor.Lines.UnitPrice = dVal;
                                    }
                                }
                                lRetCode = oPor.Update();
                                if (lRetCode != 0)
                                {
                                    CompanyInfo.Company.GetLastError(out lErrCode, out sErrMsg);
                                    sValTmp = $"- OP {sDocNumOP} OA {sDocNumOA}: tentativo di aggiornamento: " + sErrMsg + " (" + lErrCode + ").";
                                    sMsgMain = sMsgMain + sValTmp + "\r\n";
                                    AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                    return;
                                }
                                else
                                {
                                    sValTmp = $"- OP {sDocNumOP} OA {sDocNumOA}: aggiornato.";
                                    sMsgMain = sMsgMain + sValTmp + "\r\n";
                                    AppInfo.Application.StatusBar.SetText(sValTmp, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                }
                                //}
                            }
                            //
                            oRecordSet1.MoveNext();
                        }
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet1);
                        oRecordSet1 = null;
                        #endregion


                        oRecordSet.MoveNext(); //OP Successivo
                    }

                    AppInfo.Application.MessageBox(sMsgMain, 1, "Ok", "", "");
                }
                SSACommon.Disposer.ReleaseComObject(oRecordSet);
                oRecordSet = null;
                GC.Collect();
                AppInfo.Application.StatusBar.SetText(@"Operazione conclusa!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                AppInfo.Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        protected override void ItemEvent(string FormUID, ref ItemEvent pVal, ref bool BubbleEvent)
        {
            // aggiunta di un pulsante all'apertura della form
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DRAW && pVal.BeforeAction == true)
            {
                _btnExport.Visible = true;
                _btnExport.Enabled = true;
                _btnExport.Top = MyForm.Items.Item("70").Top + 25;   // inline with the object 70
                _btnExport.Left = MyForm.Items.Item("70").Left;
                _btnExport.Height = 20;
                _btnExport.Width = 80;
                _btnExport.FromPane = 0;
                _btnExport.ToPane = 0;
                _uIdBtn = _btnExport.UniqueID;
                _oBtn.Caption = "Propagatore";
                _oBtn.Type = SAPbouiCOM.BoButtonTypes.bt_Caption;
            }

            /*if (pVal.EventType == BoEventTypes.et_FORM_DATA_LOAD && pVal.BeforeAction == true && pVal.ActionSuccess == true)
            {
                _btnExport.Enabled = true;
            }
            else
            {
                _btnExport.Enabled = false;
            }*/

            /// qui gestisci le celle della matrice
            switch (pVal.EventType)
            {
                case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS:
                    {
                        try
                        {
                            if (pVal.ItemUID == "38" && (pVal.ColUID == "1" | pVal.ColUID == "11" | pVal.ColUID == "54" | pVal.ColUID == "55"))
                            {
                                sItemCodeGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("1").Cells.Item(pVal.Row).Specific).Value;
                                sQuanGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("11").Cells.Item(pVal.Row).Specific).Value;
                                sAlteGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("55").Cells.Item(pVal.Row).Specific).Value;
                                sLargGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("54").Cells.Item(pVal.Row).Specific).Value;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    break;
                case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS:
                    {
                        SAPbobsCOM.Recordset oRecordSet = null;
                        try
                        {
                            if (pVal.ItemUID == "38" && (pVal.ColUID == "1" | pVal.ColUID == "11" | pVal.ColUID == "54" | pVal.ColUID == "55"))
                            {
                                SAPbouiCOM.EditText oEdtxt = null;
                                string sItemCodeTMP = "", sQuanTMP = "", sLargTMP = "", sAlteTMP = "";
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("1").Cells.Item(pVal.Row).Specific;
                                sItemCodeTMP = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("11").Cells.Item(pVal.Row).Specific;
                                sQuanTMP = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("55").Cells.Item(pVal.Row).Specific;
                                sAlteTMP = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("54").Cells.Item(pVal.Row).Specific;
                                sLargTMP = oEdtxt.Value;
                                if ((sItemCodeGLOB == sItemCodeTMP) //   esce nel caso di nessuna modifica o modifica che lascia uguale il valore
                                    & (sQuanGLOB == sQuanTMP)
                                    & (sAlteGLOB == sAlteTMP)
                                    & (sLargGLOB == sLargTMP))
                                {
                                    return;
                                }

                                // Create a NumberFormatInfo object and set some of its properties.
                                NumberFormatInfo provider = new NumberFormatInfo();
                                provider.NumberDecimalSeparator = ",";
                                provider.NumberGroupSeparator = ".";
                                provider.NumberGroupSizes = new int[] { 3 };

                                // Create a NumberFormatInfo object and set some of its properties.
                                NumberFormatInfo provider2 = new NumberFormatInfo();
                                provider.NumberDecimalSeparator = ".";
                                provider.NumberGroupSeparator = ",";
                                provider.NumberGroupSizes = new int[] { 3 };

                                string sVal = "", sVal1 = "", sCodeItem = "", sCodeBP = "";
                                double dVal = 0, dVal1 = 0;
                                double dAlte = 0, dAltePrec = 0, dLarg = 0, dLargPrec = 0, dPrUn = 0, dAreaMQ = 0, dAreaMQPrec = 0;
                                string sAlteUM = "mm", sLargUM = "mm", sValu = "EUR";
                                bool bTestForceUnitPrice = false;

                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("1").Cells.Item(pVal.Row).Specific;
                                sCodeItem = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)MyForm.Items.Item("4").Specific;
                                sCodeBP = oEdtxt.Value;

                                #region SELECT per cercare prezzo fuori misura CASO SCAVOLINI
                                bool bTestFuoMis1 = false;
                                double dQuanFuoMis = 0, dQuan = 0, dQuanCMQ = 0;
                                double dPrezFuoMis = 0, dPrezTOT = 0;

                                oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                string sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_CODCLI], T0.[U_SSAINFO_CODART], T0.[U_SSAINFO_PRFI], T0.[U_SSAINFO_QUAN] 
FROM [dbo].[@SSAINFO_LISFUOMIS]  T0
WHERE T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"'
ORDER BY T0.[U_SSAINFO_CODCLI], T0.[U_SSAINFO_CODART]";
                                oRecordSet.DoQuery(sSQL);
                                while (!(oRecordSet.EoF))
                                {
                                    bTestFuoMis1 = true;
                                    sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                    //Quantità
                                    dQuanFuoMis = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_QUAN").Value);
                                    dPrezFuoMis = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_PRFI").Value);
                                    oRecordSet.MoveNext();
                                }
                                #endregion

                                #region Quantità
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("11").Cells.Item(pVal.Row).Specific;
                                sVal = oEdtxt.Value;
                                if (sVal != "")
                                {
                                    dQuan = double.Parse(sVal, provider2);
                                }
                                #endregion

                                #region Altezza '1.005,36mm
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("55").Cells.Item(pVal.Row).Specific;
                                sVal = oEdtxt.Value;
                                if (sVal != "")
                                {
                                    sVal1 = Tools.Right(sVal, 2);
                                    if (sVal1 != sAlteUM)
                                    {
                                        _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                                        sVal = "L'Unità di Misura dell'altezza non è quella predefinita: 'mm'.";
                                        SSACommon.AppInfo.Application.StatusBar.SetText(sVal, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)_mt);
                                        return;
                                    }
                                    sVal = Tools.Left(sVal, sVal.Length - 2);
                                    //dAlte = double.Parse(sVal, provider);
                                    dAlte = Convert.ToDouble(sVal);
                                }
                                #endregion

                                #region Larghezza
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("54").Cells.Item(pVal.Row).Specific;
                                sVal = oEdtxt.Value;
                                if (sVal != "")
                                {
                                    sVal1 = Tools.Right(sVal, 2);
                                    if (sVal1 != sLargUM)
                                    {
                                        _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                                        sVal = "L'Unità di Misura della larghezza non è quella predefinita: 'mm'.";
                                        SSACommon.AppInfo.Application.StatusBar.SetText(sVal, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)_mt);
                                        return;
                                    }
                                    sVal = Tools.Left(sVal, sVal.Length - 2);
                                    //dLarg = double.Parse(sVal, provider);
                                    dLarg = Convert.ToDouble(sVal);
                                }
                                #endregion

                                if (dQuan != 0)
                                {
                                    #region Prezzo Unitario
                                    oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("14").Cells.Item(pVal.Row).Specific;
                                    sVal = oEdtxt.Value;
                                    if (sVal != "")
                                    {
                                        string[] lines = Regex.Split(sVal, " ");
                                        sValu = lines[1];
                                        sVal = lines[0];
                                        //dPrUn = double.Parse(sVal, provider);
                                        dPrUn = Convert.ToDouble(sVal);
                                    }
                                    #endregion
                                    if (bTestFuoMis1)
                                    {
                                        dQuanCMQ = dQuan * dAlte * dLarg / 100;

                                        bool bTestFuoMis2 = false;
                                        dPrezTOT = 0;
                                        if ((dQuanCMQ > 0) & (dQuanCMQ <= dQuanFuoMis))
                                        {
                                            dPrezTOT = dPrezFuoMis;
                                            bTestFuoMis2 = true;
                                        }
                                        if (!bTestFuoMis2)
                                        {
                                            dVal = 0;
                                            oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                            sSQL = @"SELECT T0.[ItemCode], T0.[CardCode], T1.[FromDate], T1.[ToDate], T1.[Price] 
FROM OSPP T0  
INNER JOIN SPP1 T1 ON T0.[CardCode] = T1.[CardCode] and T0.[ItemCode] = T1.[ItemCode] 
WHERE T0.[ItemCode]='" + sCodeItem + @"' and  T0.[CardCode] ='" + sCodeBP + @"' and  ((T1.[ToDate] IS NULL) OR (GETDATE() BETWEEN  T1.[FromDate]  AND  T1.[ToDate] ))";
                                            oRecordSet.DoQuery(sSQL);
                                            while (!(oRecordSet.EoF))
                                            {
                                                dVal = Convert.ToDouble(oRecordSet.Fields.Item("Price").Value);
                                                //
                                                oRecordSet.MoveNext();
                                            }

                                            dPrezTOT = dVal * dQuanCMQ;
                                        }
                                        //Imposto il prezzo fisso nella colonna "Totale DI"
                                        sVal = dPrezTOT.ToString().Trim();
                                        /*
                                        int pos = sVal.IndexOf(",", 0);
                                        if (pos > 0)
                                        {
                                            sVal = sVal.Replace(",", ".");
                                        }
                                         */
                                        oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("21").Cells.Item(pVal.Row).Specific;
                                        oEdtxt.Value = sVal;
                                    }
                                    else
                                    {
                                        dVal = 0;
                                        double dList = 0;
                                        oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_LIST],isnull(T0.[U_SSAINFO_ALTE],0) as U_SSAINFO_ALTE, isnull(T0.[U_SSAINFO_LARG],0) as U_SSAINFO_LARG
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[U_SSAINFO_LIST]>=0 AND T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"'
and isnull(T0.[U_SSAINFO_ALTE],0)<>0 and isnull(T0.[U_SSAINFO_LARG],0)<>0
ORDER BY T0.[U_SSAINFO_ALTE] DESC, T0.[U_SSAINFO_LARG] DESC";
                                        oRecordSet.DoQuery(sSQL);
                                        while (!(oRecordSet.EoF))
                                        {
                                            //
                                            sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                            //Controllo sull'altezza
                                            dVal = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_ALTE").Value);
                                            //if (dVal > dAlte)
                                            if (dVal < dAlte)
                                            {
                                                break;
                                            }
                                            if (dVal != dAltePrec)
                                            {
                                                dList = 0;
                                            }
                                            //Controllo sulla larghezza     
                                            dVal1 = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_LARG").Value);
                                            //se ci si trova alla stessa altezza con larghezza superiore vuol dire che non devo prendere il considerazione il range corrente
                                            //perchè successivo
                                            //se ci si trova alla stessa altezza con larghezza inferiore vuol dire che non devo prendere il considerazione il range corrente
                                            //if ((dVal == dAltePrec) & (dVal1 > dLarg))
                                            if ((dVal == dAltePrec) & (dVal1 < dLarg))
                                            {

                                            }
                                            else
                                            {
                                                //if ((dVal != dAltePrec) & (dVal1 > dLarg)) //se è cambiata l'altezza e la larghezza è inferiore allora NON esiste il range
                                                if ((dVal != dAltePrec) & (dVal1 < dLarg)) //se è cambiata l'altezza e la larghezza è superiore allora NON esiste il range                                  
                                                {

                                                }
                                                else
                                                {
                                                    dList = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_LIST").Value);
                                                }
                                            }
                                            dAltePrec = dVal;
                                            dLargPrec = dVal1;
                                            //
                                            oRecordSet.MoveNext();
                                        }

                                        //Prezzo Unitario 
                                        dVal = dPrUn;
                                        if (dList != 0)
                                        {
                                            bTestForceUnitPrice = true;
                                            dVal = dList;
                                        }
                                        sVal = dVal.ToString().Trim();
                                        /*
                                        int pos = sVal.IndexOf(",", 0);
                                        if (pos > 0)
                                        {
                                            sVal = sVal.Replace(",", ".");
                                        }
                                         */
                                        if (bTestForceUnitPrice)
                                        {
                                            //Prz. Un.
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("14").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = sVal;
                                            //Sconto
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("15").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = "0";
                                        }
                                    }
                                    if (!bTestForceUnitPrice) //Verifica AREA MQ
                                    {
                                        //Calcolo dAreaMQ
                                        dAreaMQ = (dAlte / 1000) * (dLarg / 1000);
                                        dVal = 0;
                                        double dList = 0;
                                        oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_LIST],T0.[U_SSAINFO_AREAMQ]
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[U_SSAINFO_LIST]>=0 AND T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"'
and isnull(T0.[U_SSAINFO_AREAMQ],0)<>0
ORDER BY T0.[U_SSAINFO_AREAMQ] DESC";
                                        oRecordSet.DoQuery(sSQL);
                                        while (!(oRecordSet.EoF))
                                        {
                                            sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                            //Controllo sull'AREA MQ
                                            dVal = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_AREAMQ").Value);
                                            dList = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_LIST").Value);
                                            //if (dVal > dAlte)
                                            if (dVal < dAreaMQ)
                                            {
                                                break;
                                            }
                                            dAreaMQPrec = dVal;
                                            //
                                            oRecordSet.MoveNext();
                                        }

                                        //Prezzo Unitario 
                                        dVal = 0;
                                        if (dList != 0)
                                        {
                                            dVal = dList * dAreaMQ;
                                            bTestForceUnitPrice = true;
                                        }
                                        sVal = dVal.ToString().Trim();
                                        /*
                                        int pos = sVal.IndexOf(",", 0);
                                        if (pos > 0)
                                        {
                                            sVal = sVal.Replace(",", ".");
                                        }
                                         */
                                        if (bTestForceUnitPrice)
                                        {
                                            //Prz. Un.
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("14").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = sVal;
                                            //Sconto
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("15").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = "0";
                                        }
                                    }
                                    if (bTestForceUnitPrice)
                                    {
                                        _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                                        SSACommon.AppInfo.Application.StatusBar.SetText("Add-on [" + AddonInfo.AddonProvider + "] " + AddonInfo.AddonName + " inizializzato e collegato alla company " + AdminInfo.CompanyName() + "(" + SSACommon.AppInfo.Application.Company.DatabaseName + ")", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                    }
                                }
                                sItemCodeGLOB = sItemCodeTMP;
                                sQuanGLOB = sQuanTMP;
                                sAlteGLOB = sAlteTMP;
                                sLargGLOB = sLargTMP;
                            }
                        }
                        catch (Exception ex)
                        {
                            Tools.LogError(ex, $"Errore nel processo di popolamento delle celle della matrice, {ex.Message}");
                        }
                        finally
                        {
                            Tools.ObjectRelease(oRecordSet);
                        }
                    }
                    break;
            }
        }

        protected override void MenuEvent(ref MenuEvent pVal, ref bool BubbleEvent)
        {
            //try
            //{
            //    menu PROPAGA COMMESSA SU TUTTE LE RIGHE
            //    if (pVal.MenuUID == "SSAINFO_ELMA_NomeMenu"
            //        && MyForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE
            //        && pVal.BeforeAction == false
            //        && (MyDBDataSources("OPOR").GetValue("CardCode", 0)) != ""
            //        )
            //    {
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Utility.Tools.LogError(ex, $"Errore apertura menu, messaggio: {ex.Message}");
            //}
        }

        protected override void RightClickEvent(ref ContextMenuInfo eventInfo, ref bool BubbleEvent)
        {
            //try
            //{
            //    // attiviazione menu
            //    if (/*MyForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE && */(MyDBDataSources("OPOR").GetValue("CardCode", 0)) != "")
            //    {
            //        if (eventInfo.BeforeAction == true)
            //        {
            //            SAPbouiCOM.Menus oMenus = null;
            //            oMenus = AppInfo.Application.Menus;
            //            SAPbouiCOM.MenuItem oMenuItem = null;
            //            oMenuItem = oMenus.Item("1280"); // Data'
            //            oMenus = oMenuItem.SubMenus;

            //            if (!oMenuItem.SubMenus.Exists("SSAINFO_ELMA_NomeMenu"))
            //            {
            //                MenuInfo.CreateContextMenu(oMenuItem, BoMenuType.mt_STRING, "SSAINFO_ELMA_NomeMenu", "Menu personalizzato", true, 999);
            //            }
            //        }
            //        else
            //        {
            //            MenuInfo.RemoveContextMenu("SSAINFO_ELMA_NomeMenu");
            //        }

            // Focus di una cella
            //omat.Columns.Item("col name").Cells.Item(row number).Click()
            //    }
            //}
            //catch (Exception ex)
            //{
            //    AppInfo.Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            //}
        }
    }
}
