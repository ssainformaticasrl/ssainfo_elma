﻿using SAPbouiCOM.Framework;
using System;
using System.Collections.Generic;
using System.Xml;

namespace SSAINFO_ELMA
{
    [FormAttribute("SSAINFO_ELMA.Form1", "Forms/Form1.b1f")]
    class Form1 : UserFormBase
    {
        public Form1()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }
    }
}