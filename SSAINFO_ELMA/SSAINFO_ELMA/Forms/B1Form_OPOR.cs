﻿using SAPbouiCOM;
using Serilog;
using SSACommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAINFO_ELMA.Form
{
    /// <summary>
    /// Classe d'esempio per gestire alcuni controlli\eventi SAP tramite UI (lasciati commentati)
    /// </summary>
    class B1Form_OPOR : Base.Form.SBOSystemFormBase
    {
        //private string _globalCommValue;
        //private string _globalMatrValue;
        //private string _pointedLineComValue;

        //private SAPbouiCOM.EditText _editTextComm;
        //private SAPbouiCOM.EditText _editTextMatr;
        //private SAPbouiCOM.Matrix _matrixLines;
        //private SAPbouiCOM.Form _myFormUser; //Form dei campi utente di testata

        public B1Form_OPOR(string formUID)
            : base(formUID)
        {
            InitForm();
        }

        protected override void Dispose()
        {
        }

        protected override void FormDataEvent(ref BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent)
        {

        }

        private void ExtractDataFromUi()
        {
            //try
            //{
            //    AppInfo.Application.ActivateMenuItem("6913");                   // apre area campi utente (menu)
            //    _editTextComm = ((SAPbouiCOM.EditText)MyUDFForm.Items.Item("U_SSAINFO_COM").Specific);
            //    _matrixLines = (SAPbouiCOM.Matrix)MyForm.Items.Item("38").Specific;
            //    _globalCommValue = _editTextComm.Value;
            //}
            //catch (Exception ex)
            //{
            //}
        }

        protected override void InitForm()
        {
            //try
            //{
            //    _matrixLines = (SAPbouiCOM.Matrix)MyForm.Items.Item("38").Specific;
            //}
            //catch (Exception ex)
            //{
            //    Log.Error($"Errore nell'estrazione matrice via UI API, messaggio: {ex.Message}");
            //}
        }

        protected override void ItemEvent(string FormUID, ref ItemEvent pVal, ref bool BubbleEvent)
        {
            //// aggiunta di un pulsante all'apertura della form
            //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DRAW && pVal.BeforeAction == true)
            //{
            //    _btnExport.Visible = true;
            //    _btnExport.Enabled = false;
            //    _btnExport.Top = MyForm.Items.Item("1").Top;   // inline with the OK button
            //    _btnExport.Left = MyForm.Width / 6;
            //    _btnExport.Height = 20;
            //    _btnExport.Width = 80;
            //    _btnExport.FromPane = 0;
            //    _btnExport.ToPane = 0;
            //    _uIdBtn = _btnExport.UniqueID;
            //    _oBtn.Caption = "Export Data";
            //    _oBtn.Type = SAPbouiCOM.BoButtonTypes.bt_Caption;
            //}
        }

        protected override void MenuEvent(ref MenuEvent pVal, ref bool BubbleEvent)
        {
            //try
            //{
            //    menu PROPAGA COMMESSA SU TUTTE LE RIGHE
            //    if (pVal.MenuUID == "SSAINFO_ELMA_NomeMenu"
            //        && MyForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE
            //        && pVal.BeforeAction == false
            //        && (MyDBDataSources("OPOR").GetValue("CardCode", 0)) != ""
            //        )
            //    {
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Utility.Tools.LogError(ex, $"Errore apertura menu, messaggio: {ex.Message}");
            //}
        }

        protected override void RightClickEvent(ref ContextMenuInfo eventInfo, ref bool BubbleEvent)
        {
            //try
            //{
            //    // attiviazione menu
            //    if (/*MyForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE && */(MyDBDataSources("OPOR").GetValue("CardCode", 0)) != "")
            //    {
            //        if (eventInfo.BeforeAction == true)
            //        {
            //            SAPbouiCOM.Menus oMenus = null;
            //            oMenus = AppInfo.Application.Menus;
            //            SAPbouiCOM.MenuItem oMenuItem = null;
            //            oMenuItem = oMenus.Item("1280"); // Data'
            //            oMenus = oMenuItem.SubMenus;

            //            if (!oMenuItem.SubMenus.Exists("SSAINFO_ELMA_NomeMenu"))
            //            {
            //                MenuInfo.CreateContextMenu(oMenuItem, BoMenuType.mt_STRING, "SSAINFO_ELMA_NomeMenu", "Menu personalizzato", true, 999);
            //            }
            //        }
            //        else
            //        {
            //            MenuInfo.RemoveContextMenu("SSAINFO_ELMA_NomeMenu");
            //        }

            // Focus di una cella
            //omat.Columns.Item("col name").Cells.Item(row number).Click()
            //    }
            //}
            //catch (Exception ex)
            //{
            //    AppInfo.Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            //}
        }
    }
}
