﻿using SAPbouiCOM;
using Serilog;
using SSACommon;
using SSAINFO_ELMA.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SSAINFO_ELMA.Form
{
    /// <summary>
    /// Classe d'esempio per gestire alcuni controlli\eventi SAP tramite UI (lasciati commentati)
    /// </summary>
    class B1Form_OQUT : Base.Form.SBOSystemFormBase
    {
        //private string _globalCommValue;
        //private string _globalMatrValue;
        //private string _pointedLineComValue;

        //private SAPbouiCOM.EditText _editTextComm;
        //private SAPbouiCOM.EditText _editTextMatr;
        private SAPbouiCOM.Matrix _matrixLines;
        private SAPbouiCOM.BoStatusBarMessageType _mt;
        //private SAPbouiCOM.Form _myFormUser; //Form dei campi utente di testata
        public string sItemCodeGLOB = "";
        public string sLargGLOB = "";
        public string sAlteGLOB = "";
        public string sQuanGLOB = "";

        public B1Form_OQUT(string formUID)
            : base(formUID)
        {
            InitForm();
        }

        protected override void Dispose()
        {
        }

        protected override void FormDataEvent(ref BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent)
        {

        }

        private void ExtractDataFromUi()
        {
            try
            {
                //AppInfo.Application.ActivateMenuItem("6913");                   // apre area campi utente (menu)
                //_editTextComm = ((SAPbouiCOM.EditText)MyUDFForm.Items.Item("U_SSAINFO_COM").Specific);
                _matrixLines = (SAPbouiCOM.Matrix)MyForm.Items.Item("38").Specific;
                //_globalCommValue = _editTextComm.Value;
            }
            catch (Exception ex)
            {
            }
        }

        protected override void InitForm()
        {
            try
            {
                ExtractDataFromUi();
            }
            catch (Exception ex)
            {
                Tools.LogError(ex, $"Errore nell'estrazione matrice via UI API, messaggio: {ex.Message}");
            }
        }

        protected override void ItemEvent(string FormUID, ref ItemEvent pVal, ref bool BubbleEvent)
        {
            /// qui gestisci le celle della matrice
            switch (pVal.EventType)
            {
                case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS:
                    {
                        try
                        {
                            if (pVal.ItemUID == "38" && (pVal.ColUID == "1" | pVal.ColUID == "11" | pVal.ColUID == "54" | pVal.ColUID == "55"))
                            {
                                sItemCodeGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("1").Cells.Item(pVal.Row).Specific).Value;
                                sQuanGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("11").Cells.Item(pVal.Row).Specific).Value;
                                sAlteGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("55").Cells.Item(pVal.Row).Specific).Value;
                                sLargGLOB = ((SAPbouiCOM.EditText)_matrixLines.Columns.Item("54").Cells.Item(pVal.Row).Specific).Value;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    break;
                case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS:
                    {
                        SAPbobsCOM.Recordset oRecordSet = null;
                        try
                        {
                            if (pVal.ItemUID == "38" && (pVal.ColUID == "1" | pVal.ColUID == "11" | pVal.ColUID == "54" | pVal.ColUID == "55"))
                            {
                                SAPbouiCOM.EditText oEdtxt = null;
                                string sItemCodeTMP = "", sQuanTMP = "", sLargTMP = "", sAlteTMP = "";
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("1").Cells.Item(pVal.Row).Specific;
                                sItemCodeTMP = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("11").Cells.Item(pVal.Row).Specific;
                                sQuanTMP = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("55").Cells.Item(pVal.Row).Specific;
                                sAlteTMP = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("54").Cells.Item(pVal.Row).Specific;
                                sLargTMP = oEdtxt.Value;
                                if ((sItemCodeGLOB == sItemCodeTMP) //   esce nel caso di nessuna modifica o modifica che lascia uguale il valore
                                    & (sQuanGLOB == sQuanTMP)
                                    & (sAlteGLOB == sAlteTMP)
                                    & (sLargGLOB == sLargTMP))
                                {
                                    return;
                                }

                                // Create a NumberFormatInfo object and set some of its properties.
                                NumberFormatInfo provider = new NumberFormatInfo();
                                provider.NumberDecimalSeparator = ",";
                                provider.NumberGroupSeparator = ".";
                                provider.NumberGroupSizes = new int[] { 3 };

                                // Create a NumberFormatInfo object and set some of its properties.
                                NumberFormatInfo provider2 = new NumberFormatInfo();
                                provider.NumberDecimalSeparator = ".";
                                provider.NumberGroupSeparator = ",";
                                provider.NumberGroupSizes = new int[] { 3 };

                                string sVal = "", sVal1 = "", sCodeItem = "", sCodeBP = "";
                                double dVal = 0, dVal1 = 0;
                                double dAlte = 0, dAltePrec = 0, dLarg = 0, dLargPrec = 0, dPrUn = 0, dAreaMQ = 0, dAreaMQPrec = 0;
                                string sAlteUM = "mm", sLargUM = "mm", sValu = "EUR";
                                bool bTestForceUnitPrice = false;

                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("1").Cells.Item(pVal.Row).Specific;
                                sCodeItem = oEdtxt.Value;
                                oEdtxt = (SAPbouiCOM.EditText)MyForm.Items.Item("4").Specific;
                                sCodeBP = oEdtxt.Value;

                                #region SELECT per cercare prezzo fuori misura CASO SCAVOLINI
                                bool bTestFuoMis1 = false;
                                double dQuanFuoMis = 0, dQuan = 0, dQuanCMQ = 0;
                                double dPrezFuoMis = 0, dPrezTOT = 0;

                                oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                string sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_CODCLI], T0.[U_SSAINFO_CODART], T0.[U_SSAINFO_PRFI], T0.[U_SSAINFO_QUAN] 
FROM [dbo].[@SSAINFO_LISFUOMIS]  T0
WHERE T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"'
ORDER BY T0.[U_SSAINFO_CODCLI], T0.[U_SSAINFO_CODART]";
                                oRecordSet.DoQuery(sSQL);
                                while (!(oRecordSet.EoF))
                                {
                                    bTestFuoMis1 = true;
                                    sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                    //Quantità
                                    dQuanFuoMis = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_QUAN").Value);
                                    dPrezFuoMis = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_PRFI").Value);
                                    oRecordSet.MoveNext();
                                }
                                #endregion

                                #region Quantità
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("11").Cells.Item(pVal.Row).Specific;
                                sVal = oEdtxt.Value;
                                if (sVal != "")
                                {
                                    dQuan = double.Parse(sVal, provider2);
                                }
                                #endregion

                                #region Altezza '1.005,36mm
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("55").Cells.Item(pVal.Row).Specific;
                                sVal = oEdtxt.Value;
                                if (sVal != "")
                                {
                                    sVal1 = Tools.Right(sVal, 2);
                                    if (sVal1 != sAlteUM)
                                    {
                                        _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                                        sVal = "L'Unità di Misura dell'altezza non è quella predefinita: 'mm'.";
                                        SSACommon.AppInfo.Application.StatusBar.SetText(sVal, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)_mt);
                                        return;
                                    }
                                    sVal = Tools.Left(sVal, sVal.Length - 2);
                                    //dAlte = double.Parse(sVal, provider);
                                    dAlte = Convert.ToDouble(sVal);
                                }
                                #endregion

                                #region Larghezza
                                oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("54").Cells.Item(pVal.Row).Specific;
                                sVal = oEdtxt.Value;
                                if (sVal != "")
                                {
                                    sVal1 = Tools.Right(sVal, 2);
                                    if (sVal1 != sLargUM)
                                    {
                                        _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                                        sVal = "L'Unità di Misura della larghezza non è quella predefinita: 'mm'.";
                                        SSACommon.AppInfo.Application.StatusBar.SetText(sVal, SAPbouiCOM.BoMessageTime.bmt_Short, (SAPbouiCOM.BoStatusBarMessageType)_mt);
                                        return;
                                    }
                                    sVal = Tools.Left(sVal, sVal.Length - 2);
                                    //dLarg = double.Parse(sVal, provider);
                                    dLarg = Convert.ToDouble(sVal);
                                }
                                #endregion

                                if (dQuan != 0)
                                {
                                    #region Prezzo Unitario
                                    oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("14").Cells.Item(pVal.Row).Specific;
                                    sVal = oEdtxt.Value;
                                    if (sVal != "")
                                    {
                                        string[] lines = Regex.Split(sVal, " ");
                                        sValu = lines[1];
                                        sVal = lines[0];
                                        //dPrUn = double.Parse(sVal, provider);
                                        dPrUn = Convert.ToDouble(sVal);
                                    }
                                    #endregion
                                    if (bTestFuoMis1)
                                    {
                                        dQuanCMQ = dQuan * dAlte * dLarg / 100;

                                        bool bTestFuoMis2 = false;
                                        dPrezTOT = 0;
                                        if ((dQuanCMQ > 0) & (dQuanCMQ <= dQuanFuoMis))
                                        {
                                            dPrezTOT = dPrezFuoMis;
                                            bTestFuoMis2 = true;
                                        }
                                        if (!bTestFuoMis2)
                                        {
                                            dVal = 0;
                                            oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                            sSQL = @"SELECT T0.[ItemCode], T0.[CardCode], T1.[FromDate], T1.[ToDate], T1.[Price] 
FROM OSPP T0  
INNER JOIN SPP1 T1 ON T0.[CardCode] = T1.[CardCode] and T0.[ItemCode] = T1.[ItemCode] 
WHERE T0.[ItemCode]='" + sCodeItem + @"' and  T0.[CardCode] ='" + sCodeBP + @"' and  ((T1.[ToDate] IS NULL) OR (GETDATE() BETWEEN  T1.[FromDate]  AND  T1.[ToDate] ))";
                                            oRecordSet.DoQuery(sSQL);
                                            while (!(oRecordSet.EoF))
                                            {
                                                dVal = Convert.ToDouble(oRecordSet.Fields.Item("Price").Value);
                                                //
                                                oRecordSet.MoveNext();
                                            }

                                            dPrezTOT = dVal * dQuanCMQ;
                                        }
                                        //Imposto il prezzo fisso nella colonna "Totale DI"
                                        sVal = dPrezTOT.ToString().Trim();
                                        /*
                                        int pos = sVal.IndexOf(",", 0);
                                        if (pos > 0)
                                        {
                                            sVal = sVal.Replace(",", ".");
                                        }
                                         */
                                        oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("21").Cells.Item(pVal.Row).Specific;
                                        oEdtxt.Value = sVal;
                                    }
                                    else
                                    {
                                        dVal = 0;
                                        double dList = 0;
                                        oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_LIST],isnull(T0.[U_SSAINFO_ALTE],0) as U_SSAINFO_ALTE, isnull(T0.[U_SSAINFO_LARG],0) as U_SSAINFO_LARG
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[U_SSAINFO_LIST]>=0 AND T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"'
and isnull(T0.[U_SSAINFO_ALTE],0)<>0 and isnull(T0.[U_SSAINFO_LARG],0)<>0
ORDER BY T0.[U_SSAINFO_ALTE] DESC, T0.[U_SSAINFO_LARG] DESC";
                                        oRecordSet.DoQuery(sSQL);
                                        while (!(oRecordSet.EoF))
                                        {
                                            //
                                            sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                            //Controllo sull'altezza
                                            dVal = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_ALTE").Value);
                                            //if (dVal > dAlte)
                                            if (dVal < dAlte)
                                            {
                                                break;
                                            }
                                            if (dVal != dAltePrec)
                                            {
                                                dList = 0;
                                            }
                                            //Controllo sulla larghezza     
                                            dVal1 = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_LARG").Value);
                                            //se ci si trova alla stessa altezza con larghezza superiore vuol dire che non devo prendere il considerazione il range corrente
                                            //perchè successivo
                                            //se ci si trova alla stessa altezza con larghezza inferiore vuol dire che non devo prendere il considerazione il range corrente
                                            //if ((dVal == dAltePrec) & (dVal1 > dLarg))
                                            if ((dVal == dAltePrec) & (dVal1 < dLarg))
                                            {

                                            }
                                            else
                                            {
                                                //if ((dVal != dAltePrec) & (dVal1 > dLarg)) //se è cambiata l'altezza e la larghezza è inferiore allora NON esiste il range
                                                if ((dVal != dAltePrec) & (dVal1 < dLarg)) //se è cambiata l'altezza e la larghezza è superiore allora NON esiste il range                                  
                                                {

                                                }
                                                else
                                                {
                                                    dList = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_LIST").Value);
                                                }
                                            }
                                            dAltePrec = dVal;
                                            dLargPrec = dVal1;
                                            //
                                            oRecordSet.MoveNext();
                                        }

                                        //Prezzo Unitario 
                                        dVal = dPrUn;
                                        if (dList != 0)
                                        {
                                            bTestForceUnitPrice = true;
                                            dVal = dList;
                                        }
                                        sVal = dVal.ToString().Trim();
                                        /*
                                        int pos = sVal.IndexOf(",", 0);
                                        if (pos > 0)
                                        {
                                            sVal = sVal.Replace(",", ".");
                                        }
                                         */
                                        if (bTestForceUnitPrice)
                                        {
                                            //Prz. Un.
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("14").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = sVal;
                                            //Sconto
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("15").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = "0";
                                        }
                                    }
                                    if (!bTestForceUnitPrice) //Verifica AREA MQ
                                    {
                                        //Calcolo dAreaMQ
                                        dAreaMQ = (dAlte / 1000) * (dLarg / 1000);
                                        dVal = 0;
                                        double dList = 0;
                                        oRecordSet = (SAPbobsCOM.Recordset)CompanyInfo.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                        sSQL = @"SELECT T0.[Code],T0.[U_SSAINFO_LIST],T0.[U_SSAINFO_AREAMQ]
FROM [dbo].[@SSAINFO_LISALTLAR]  T0 
WHERE T0.[U_SSAINFO_LIST]>=0 AND T0.[U_SSAINFO_CODCLI] ='" + sCodeBP + @"' and  T0.[U_SSAINFO_CODART] ='" + sCodeItem + @"'
and isnull(T0.[U_SSAINFO_AREAMQ],0)<>0
ORDER BY T0.[U_SSAINFO_AREAMQ] DESC";
                                        oRecordSet.DoQuery(sSQL);
                                        while (!(oRecordSet.EoF))
                                        {
                                            sVal = oRecordSet.Fields.Item("Code").Value.ToString().Trim();
                                            //Controllo sull'AREA MQ
                                            dVal = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_AREAMQ").Value);
                                            dList = Convert.ToDouble(oRecordSet.Fields.Item("U_SSAINFO_LIST").Value);
                                            //if (dVal > dAlte)
                                            if (dVal < dAreaMQ)
                                            {
                                                break;
                                            }
                                            dAreaMQPrec = dVal;
                                            //
                                            oRecordSet.MoveNext();
                                        }

                                        //Prezzo Unitario 
                                        dVal = 0;
                                        if (dList != 0)
                                        {
                                            dVal = dList * dAreaMQ;
                                            bTestForceUnitPrice = true;
                                        }
                                        sVal = dVal.ToString().Trim();
                                        /*
                                        int pos = sVal.IndexOf(",", 0);
                                        if (pos > 0)
                                        {
                                            sVal = sVal.Replace(",", ".");
                                        }
                                         */
                                        if (bTestForceUnitPrice)
                                        {
                                            //Prz. Un.
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("14").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = sVal;
                                            //Sconto
                                            oEdtxt = (SAPbouiCOM.EditText)_matrixLines.Columns.Item("15").Cells.Item(pVal.Row).Specific;
                                            oEdtxt.Value = "0";
                                        }
                                    }
                                    if (bTestForceUnitPrice)
                                    {
                                        _mt = SAPbouiCOM.BoStatusBarMessageType.smt_Warning;
                                        SSACommon.AppInfo.Application.StatusBar.SetText("Add-on [" + AddonInfo.AddonProvider + "] " + AddonInfo.AddonName + " inizializzato e collegato alla company " + AdminInfo.CompanyName() + "(" + SSACommon.AppInfo.Application.Company.DatabaseName + ")", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                    }
                                }
                                sItemCodeGLOB = sItemCodeTMP;
                                sQuanGLOB = sQuanTMP;
                                sAlteGLOB = sAlteTMP;
                                sLargGLOB = sLargTMP;
                            }
                        }
                        catch (Exception ex)
                        {
                            Tools.LogError(ex, $"Errore nel processo di popolamento delle celle della matrice, {ex.Message}");
                        }
                        finally
                        {
                            Tools.ObjectRelease(oRecordSet);
                        }
                    }
                    break;
            }
        }

        protected override void MenuEvent(ref MenuEvent pVal, ref bool BubbleEvent)
        {
            //try
            //{
            //    menu PROPAGA COMMESSA SU TUTTE LE RIGHE
            //    if (pVal.MenuUID == "SSAINFO_ELMA_NomeMenu"
            //        && MyForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE
            //        && pVal.BeforeAction == false
            //        && (MyDBDataSources("OPOR").GetValue("CardCode", 0)) != ""
            //        )
            //    {
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Utility.Tools.LogError(ex, $"Errore apertura menu, messaggio: {ex.Message}");
            //}
        }

        protected override void RightClickEvent(ref ContextMenuInfo eventInfo, ref bool BubbleEvent)
        {
            //try
            //{
            //    // attiviazione menu
            //    if (/*MyForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE && */(MyDBDataSources("OPOR").GetValue("CardCode", 0)) != "")
            //    {
            //        if (eventInfo.BeforeAction == true)
            //        {
            //            SAPbouiCOM.Menus oMenus = null;
            //            oMenus = AppInfo.Application.Menus;
            //            SAPbouiCOM.MenuItem oMenuItem = null;
            //            oMenuItem = oMenus.Item("1280"); // Data'
            //            oMenus = oMenuItem.SubMenus;

            //            if (!oMenuItem.SubMenus.Exists("SSAINFO_ELMA_NomeMenu"))
            //            {
            //                MenuInfo.CreateContextMenu(oMenuItem, BoMenuType.mt_STRING, "SSAINFO_ELMA_NomeMenu", "Menu personalizzato", true, 999);
            //            }
            //        }
            //        else
            //        {
            //            MenuInfo.RemoveContextMenu("SSAINFO_ELMA_NomeMenu");
            //        }

            // Focus di una cella
            //omat.Columns.Item("col name").Cells.Item(row number).Click()
            //    }
            //}
            //catch (Exception ex)
            //{
            //    AppInfo.Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            //}
        }
    }
}
