﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSAINFO_ELMA.Base.Form
{
    public abstract class SBOSystemFormBase
    {

        #region " STATE "

        private SAPbouiCOM.Form m_oForm;
        private SAPbouiCOM.Form m_oUDFForm;
        private SAPbouiCOM.Form m_oParent;
        private SAPbouiCOM.Application m_oApp;

        private string m_FormUID = "";

        protected abstract void InitForm();
        protected abstract void Dispose();
        protected abstract void ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent);
        protected abstract void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent);
        protected abstract void FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent);
        protected abstract void RightClickEvent(ref SAPbouiCOM.ContextMenuInfo eventInfo, ref bool BubbleEvent);


        #endregion


        #region " BASIC "


        public SBOSystemFormBase(string formUID, string formType = "", int formCount = -1, bool HelpEnabled = false, string HelpTopic = "")
        {

            m_oApp = AppInfo.Application;
            m_oForm = m_oApp.Forms.Item(formUID);

            m_FormUID = formUID;

            m_oParent = null;
            if (!string.IsNullOrEmpty(formType) & formCount != -1)
            {
                m_oParent = m_oApp.Forms.GetForm(formType, formCount);
            }

            m_oApp.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(m_oApp_FormDataEvent);
            m_oApp.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(m_oApp_ItemEvent);
            m_oApp.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(m_oApp_MenuEvent);
            m_oApp.RightClickEvent += new SAPbouiCOM._IApplicationEvents_RightClickEventEventHandler(m_oApp_RightClickEvent);

        }

        protected SAPbouiCOM.Item GetItem(string uid)
        {
            return m_oForm.Items.Item(uid);
        }

        private void m_oApp_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (m_oForm != null && m_oApp.Forms.ActiveForm != null)
                {
                    if (m_oApp.Forms.ActiveForm.UniqueID == m_oForm.UniqueID)
                    {
                        MenuEvent(ref pVal, ref BubbleEvent);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
        }

        private void m_oApp_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (m_oForm != null)
                {
                    if (pVal.FormUID == m_oForm.UniqueID)
                    {
                        ItemEvent(FormUID, ref pVal, ref BubbleEvent);
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & !pVal.BeforeAction)
                        {
                            ReleaseBase();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
        }

        private void m_oApp_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (m_oForm != null)
                {
                    if (BusinessObjectInfo.FormUID == m_oForm.UniqueID)
                    {
                        FormDataEvent(ref BusinessObjectInfo, ref BubbleEvent);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
        }

        private void m_oApp_RightClickEvent(ref SAPbouiCOM.ContextMenuInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if ((m_oForm != null))
                {
                    if (eventInfo.FormUID == m_oForm.UniqueID)
                    {
                        RightClickEvent(ref eventInfo, ref BubbleEvent);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                m_oApp.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
        }

        public SAPbouiCOM.Form MyForm
        {

            get { return m_oForm; }

            set { m_oForm = value; }
        }

        public SAPbouiCOM.Form MyUDFForm
        {

            get
            {
                m_oUDFForm = null;
                try
                {
                    m_oUDFForm = AppInfo.Application.Forms.Item(MyForm.UDFFormUID);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                }
                return m_oUDFForm;
            }

        }


        public SAPbouiCOM.Form MyParent
        {

            get { return m_oParent; }

            set { m_oParent = value; }
        }


        protected SAPbouiCOM.DBDataSource MyDBDataSources(int index)
        {
            try
            {
                return m_oForm.DataSources.DBDataSources.Item(index);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
            return null;
        }

        protected SAPbouiCOM.DBDataSource MyDBDataSources(string datasourceID)
        {
            try
            {
                return MyForm.DataSources.DBDataSources.Item(datasourceID);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
            return null;

        }

        protected SAPbouiCOM.UserDataSource MyUserDataSources(int index)
        {

            try
            {
                return MyForm.DataSources.UserDataSources.Item(index);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
            return null;

        }

        protected SAPbouiCOM.UserDataSource MyUserDataSources(string datasourceID)
        {

            try
            {
                return MyForm.DataSources.UserDataSources.Item(datasourceID);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
            return null;
        }

        /// <summary>
        /// Get a DataTable by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected SAPbouiCOM.DataTable MyDataTables(int index)
        {
            try
            {
                return MyForm.DataSources.DataTables.Item(index);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;
        }

        /// <summary>
        /// Get a DataTable by UniqueID
        /// </summary>
        /// <param name="datasourceID"></param>
        /// <returns></returns>
        protected SAPbouiCOM.DataTable MyDataTables(string datasourceID)
        {
            try
            {
                return MyForm.DataSources.DataTables.Item(datasourceID);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;

        }

        #endregion



        #region " PRIVATE "

        protected void ReleaseBase()
        {
            try
            {
                System.Diagnostics.Trace.Write(DateTime.Now);
                Dispose();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                m_oApp.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }

            try
            {
                m_oApp.FormDataEvent -= new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(m_oApp_FormDataEvent);
                m_oApp.ItemEvent -= new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(m_oApp_ItemEvent);
                m_oApp.MenuEvent -= new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(m_oApp_MenuEvent);
                m_oApp.RightClickEvent -= new SAPbouiCOM._IApplicationEvents_RightClickEventEventHandler(m_oApp_RightClickEvent);
                m_oForm = null;
                //m_oApp = null;
                GC.Collect();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }

            GC.Collect();
        }

        #endregion

    }
}
