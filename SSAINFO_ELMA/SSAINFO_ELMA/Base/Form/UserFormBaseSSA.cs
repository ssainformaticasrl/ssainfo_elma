﻿using SAPbouiCOM.Framework;
using Serilog;
using SSACommon;
using SSAINFO_ELMA.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SSAINFO_ELMA.Base.Form
{
    /// <summary>
    /// FormBase personalizzata SSA
    /// Versioni:
    /// 1.5 (13/12/2019): 
    ///     resize della larghezza delle colonne delle matrici/griglia (ratio al quadrato)
    /// 1.4 (09/09/2019): 
    ///     commentato m_oApp = null in metodo ReleaseBase
    /// 1.3 (09/08/2019): 
    ///     release su chiusura form e rimozione delegates eventi
    ///     ItemEvent
    /// 1.2 (15/02/2019): 
    ///     gestione ridimensionamento e posizionamento Items in funzione della dimensione del font (base = Tahoma 10)
    /// 
    /// 1.1 (01/04/2018):
    ///     soppressione top, left e color su inizializzazione form (automaticamente gestiti da client SAP)
    ///     
    /// 1.0 (30/11/2016):
    ///     prima versione da framework SAP B1 Studio
    /// </summary>
    public abstract class UserFormBaseSSA : FormBase
    {

        private bool m_firstResize = true;
        //private bool m_isInitialized;
        private SAPbouiCOM.Form m_oForm;
        private string m_oParent;
        private bool m_multiForm;
        private SAPbouiCOM.Application m_oApp;

        private bool m_ModalForm; //private
        private bool m_clearPosition;
        private bool m_clearSize;
        private bool m_resizeItems;
        //----------------------------------------------------------
        private B1SFileManagerEx m_fileManager;
        //public static readonly string B1S_POSTFIX = ".b1s";
        //public static readonly string VSI = "VSIcreated";
        //private static readonly string ErrorNoB1S = "b1s file is not found!";
        //public static readonly string ATTR_FORM_TYPE = "FormType";

        //----------------------------------------------------------

        protected abstract string XmlFileName { get; }
        protected abstract string XmlFromAttribute { get; }
        protected abstract string FormType { get; }

        //protected abstract void InitForm();
        public abstract void OnInitializeComponent();
        protected abstract void Dispose();


        //Eventi
        protected abstract void RightClickEvent(ref SAPbouiCOM.ContextMenuInfo eventInfo, ref bool BubbleEvent);
        protected abstract void FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, ref bool BubbleEvent);
        protected abstract void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent);
        protected abstract void ItemEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent);


        /// <summary>
        /// Predisposizione caricamento form da file B1s della DLL corrente
        /// </summary>
        /// <param name="resource">Nome della form (form Type)</param>
        /// <param name="modal">Form in stato modale (opzionale)</param>
        /// <param name="clearPosition">Rimozione posizione fissa della form, riprendendo l'ultima posizione memorizzata dal client SAP (opzionale)</param>
        public UserFormBaseSSA(string resource, bool modal = false, bool clearPosition = true, bool clearSize = true, bool resizeItems = true)
        {
            m_oApp = Application.SBO_Application;
            m_multiForm = true; // multiForm;

            m_ModalForm = modal;
            m_clearPosition = clearPosition;
            m_clearSize = clearSize;
            m_resizeItems = resizeItems;

            if (resource != "")
            {
                CreateForm(resource, (FormBase)this);
            }
            else
            {
                CreateForm();
            }

            Application.SBO_Application.RightClickEvent += new SAPbouiCOM._IApplicationEvents_RightClickEventEventHandler(SBO_Application_RightClickEvent);
            Application.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            Application.SBO_Application.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBO_Application_FormDataEvent);
            Application.SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
        }

        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            //sul form close after, lancia il Release dell'oggetto!!!
            if (m_oForm != null)
            {
                ItemEvent(ref pVal, ref BubbleEvent);

                //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_RESIZE && pVal.BeforeAction == false && pVal.FormUID == m_oForm.UniqueID)
                //{
                //    bool innerEv = pVal.InnerEvent;
                //    int fHeight = m_oForm.Height;
                //    int fWidth = m_oForm.Width;
                //    int fMaxHeight = m_oForm.MaxHeight;
                //    int fMaxWidth = m_oForm.MaxWidth;

                //    //int fHeightExpand = Convert.ToInt32(m_oForm.Height * ratio);
                //    //if (m_firstResize)
                //    //{
                //    //    m_firstResize = false;
                //    //    //m_oForm.Resize(Convert.ToInt32(fWidth * ratio), Convert.ToInt32(fHeight * ratio));
                //    //    ResizeForm();

                //    //}
                //}

                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && pVal.BeforeAction == false && pVal.FormUID == m_oForm.UniqueID)
                {
                    Release();
                }
            }
        }

        private void SBO_Application_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if ((m_oForm != null))
                {
                    if (BusinessObjectInfo.FormUID == m_oForm.UniqueID)
                    {
                        FormDataEvent(ref BusinessObjectInfo, ref BubbleEvent);

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Application.SBO_Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
        }

        private void SBO_Application_RightClickEvent(ref SAPbouiCOM.ContextMenuInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if ((MyForm != null))
                {
                    if (eventInfo.FormUID == m_oForm.UniqueID)
                    {
                        RightClickEvent(ref eventInfo, ref BubbleEvent);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Application.SBO_Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
        }

        private void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (m_oForm != null && m_oApp.Forms.ActiveForm != null)
                {
                    if (m_oApp.Forms.ActiveForm.UniqueID == m_oForm.UniqueID)
                    {
                        MenuEvent(ref pVal, ref BubbleEvent);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((m_oForm != null))
                    m_oForm.Freeze(false);
            }
        }

        protected double ratio
        {
            get { return Convert.ToDouble(AppInfo.Application.FontHeight) / 10.00; } //prendo a riferimento tahoma 10; }
        }

        protected SAPbouiCOM.Form MyForm
        {
            get { return m_oForm; }
            set { m_oForm = value; }
        }

        protected new SAPbouiCOM.Item GetItem(string uid)
        {
            return MyForm.Items.Item(uid);
        }

        private bool CreateForm(string resource, FormBase form)
        {

            //string path = Assembly.GetEntryAssembly().GetName().Name + B1SNodeConstant.B1S_POSTFIX;
            string path = Assembly.GetExecutingAssembly().GetName().Name + B1SNodeConstant.B1S_POSTFIX;
            if (!File.Exists(path))
                return false;
            this.m_fileManager = new B1SFileManagerEx();
            this.m_fileManager.B1sPackageName = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + System.IO.Path.DirectorySeparatorChar + path;
            this.m_fileManager.Initialize();

            if (this.m_fileManager == null)
                throw new InvalidOperationException(B1SNodeConstant.ErrorNoB1S);
            string fileContent = (string)null;
            if (string.IsNullOrEmpty(resource) || !this.m_fileManager.GetFileContent(B1SNodeConstant.VSI, resource, out fileContent))
                return false;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(fileContent);
            xmlDocument.SelectSingleNode(B1SNodeConstant.AddFormNodePath).Attributes[B1SNodeConstant.ATTR_FORM_TYPE].Value = form.FormType;

            System.Xml.XmlNode formNode;
            string attribName;
            XmlAttribute attrib;

            formNode = xmlDocument.SelectSingleNode(B1SNodeConstant.AddFormNodePath);

            if (m_clearPosition)
            {
                attribName = "top"; //case sensitive
                attrib = formNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                formNode.Attributes.Remove(attrib);

                attribName = "left"; //case sensitive
                attrib = formNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                formNode.Attributes.Remove(attrib);
            }

            try
            {
                attribName = "color"; //case sensitive
                attrib = formNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                formNode.Attributes.Remove(attrib);
            }
            catch { }

            try
            {
                attribName = "Color"; //case sensitive
                attrib = formNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                formNode.Attributes.Remove(attrib);
            }
            catch { }
            //rivedere le dimensioni perchè così non funzionano
            //if (m_clearSize)
            //{
            //  attribName = "width"; //case sensitive
            //  attrib = formNode.Attributes.Cast<XmlAttribute>()
            //   .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
            //  formNode.Attributes.Remove(attrib);

            //  attribName = "height"; //case sensitive
            //  attrib = formNode.Attributes.Cast<XmlAttribute>()
            //   .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
            //  formNode.Attributes.Remove(attrib);
            //}

            if (m_ModalForm)
            {
                attribName = "modality"; //case sensitive
                attrib = formNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                formNode.Attributes.Remove(attrib);
                attrib = xmlDocument.CreateAttribute(attribName);
                attrib.Value = "1";
                formNode.Attributes.Append(attrib);
            }

            string innerXmlPre = xmlDocument.InnerXml;

            if (m_resizeItems)
            {
                try
                {
                    ResizeForm(ref xmlDocument);
                    innerXmlPre = xmlDocument.InnerXml;
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    Application.SBO_Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                }

            }

            string innerXml = innerXmlPre;

            try
            {

                SAPbouiCOM.FormCreationParams oCreationParam = default(SAPbouiCOM.FormCreationParams);

                oCreationParam = (SAPbouiCOM.FormCreationParams)AppInfo.Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

                oCreationParam.XmlData = innerXml.Replace("\\\"", "\"");
                MyForm = AppInfo.Application.Forms.AddEx(oCreationParam);

                this.OnInitializeComponent();
            }
            catch (Exception ex)
            {
                AppInfo.Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                return false;
            }
            return true;
        }

        protected void ResizeFormItems()
        {
            foreach (SAPbouiCOM.Item oItem in m_oForm.Items)
            {
                oItem.Top = Convert.ToInt32(oItem.Top * ratio);
                oItem.Left = Convert.ToInt32(oItem.Left * ratio);
                oItem.Height = Convert.ToInt32(oItem.Height * ratio);
                oItem.Width = Convert.ToInt32(oItem.Width * ratio);
            }
        }

        private void ResizeForm(ref XmlDocument xmlDocument)
        {
            System.Xml.XmlNode formNode;
            string attribName;
            XmlAttribute attrib;
            int formHeight;
            int formWidth;
            int desktopHeight = AppInfo.Application.Desktop.Height;
            int desktopWidth = AppInfo.Application.Desktop.Width;



            //sposto/ridimensiono prima gli item

            foreach (System.Xml.XmlNode formItemsNode in xmlDocument.SelectNodes("/Application/forms/action[@type='add']/form/items/action[@type='add']/item"))
            {
                attribName = "top"; //case sensitive
                attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();

                attribName = "left"; //case sensitive
                attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();

                attribName = "type"; //case sensitive
                attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                if (attrib.Value == ((int)SAPbouiCOM.BoFormItemTypes.it_MATRIX).ToString()
                    || attrib.Value == ((int)SAPbouiCOM.BoFormItemTypes.it_GRID).ToString())
                {
                    //formNode = formItemsNode.SelectSingleNode("/specific");
                    //attribName = "titleHeight"; //case sensitive
                    //attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                    // .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                    //attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();

                    //attribName = "cellHeight"; //case sensitive
                    //attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                    // .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                    //attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();

                    foreach (System.Xml.XmlNode formColumnsNode in formItemsNode.SelectNodes("specific/columns/action[@type='add']/column"))
                    {
                        attribName = "width"; //case sensitive
                        attrib = formColumnsNode.Attributes.Cast<XmlAttribute>()
                         .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                        attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString(); //meglio con il ratio al quadrato!!!
                        //formColumnsNode.Attributes.Remove(attrib);
                    }
                }

                attribName = "height"; //case sensitive
                attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();

                attribName = "width"; //case sensitive
                attrib = formItemsNode.Attributes.Cast<XmlAttribute>()
                 .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
                attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();


            }

            //poi ridimensiono la form
            formNode = xmlDocument.SelectSingleNode(B1SNodeConstant.AddFormNodePath);
            attribName = "width"; //case sensitive
            attrib = formNode.Attributes.Cast<XmlAttribute>()
             .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
            formWidth = Convert.ToInt32(double.Parse(attrib.Value) * ratio);
            attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();


            formNode = xmlDocument.SelectSingleNode(B1SNodeConstant.AddFormNodePath);
            attribName = "height"; //case sensitive
            attrib = formNode.Attributes.Cast<XmlAttribute>()
             .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
            formHeight = Convert.ToInt32(double.Parse(attrib.Value) * ratio);
            attrib.Value = (Convert.ToInt32(double.Parse(attrib.Value) * ratio)).ToString();

            formNode = xmlDocument.SelectSingleNode(B1SNodeConstant.AddFormNodePath);
            attribName = "client_width"; //case sensitive
            attrib = formNode.Attributes.Cast<XmlAttribute>()
             .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
            //attrib.Value = formWidth.ToString();
            formNode.Attributes.Remove(attrib);
            //formNode.Attributes.Cast<XmlAttribute>().Where(a => string.Compare(a.Name, "width", true) == 0).SingleOrDefault().Value = Convert.ToInt32(formWidth - 100 * ratio).ToString();
            //formNode.Attributes.Cast<XmlAttribute>().Where(a => string.Compare(a.Name, "height", true) == 0).SingleOrDefault().Value = Convert.ToInt32(formHeight - 100 * ratio).ToString();

            formNode = xmlDocument.SelectSingleNode(B1SNodeConstant.AddFormNodePath);
            attribName = "client_height"; //case sensitive
            attrib = formNode.Attributes.Cast<XmlAttribute>()
             .Where(a => string.Compare(a.Name, attribName, true) == 0).SingleOrDefault();
            //attrib.Value = formHeight.ToString();
            formNode.Attributes.Remove(attrib);

        }




        private void CreateForm()
        {


            System.Xml.XmlDocument oXmlDoc = default(System.Xml.XmlDocument);
            SAPbouiCOM.FormCreationParams oCreationParam = default(SAPbouiCOM.FormCreationParams);

            oCreationParam = null;

            try
            {
                oXmlDoc = new System.Xml.XmlDocument();

                oCreationParam = (SAPbouiCOM.FormCreationParams)AppInfo.Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

                if (!string.IsNullOrEmpty(XmlFileName))
                {
                    oXmlDoc.Load(Util.GetXmlStream(typeof(UserFormBaseSSA), XmlFileName));
                    oCreationParam.XmlData = oXmlDoc.InnerXml;
                }
                else
                {
                    //oCreationParam.BorderStyle = BorderStyle;
                }

                if (m_multiForm)
                {
                    oCreationParam.UniqueID = FormType;
                }
                else
                {
                    oCreationParam.UniqueID = FormType;
                }

                MyForm = AppInfo.Application.Forms.AddEx(oCreationParam);

                this.OnInitializeComponent();

            }
            catch (Exception ex)
            {
                // Comunque sia, se esiste, defrizza la form anche se non è stata frizzata
                if ((MyForm != null))
                    MyForm.Freeze(false);
                Log.Error(ex.Message);
                m_oApp.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }

            if ((oCreationParam != null))
            {
                Disposer.ReleaseComObject(oCreationParam);
            }

            oXmlDoc = null;
        }

        public void Show()
        {
            //this.Show();
            //if (!this.Alive)
            //  return;
            //this.UIAPIRawForm.Visible = true;
            if (MyForm == null)
                return;
            MyForm.Visible = true;

        }

        public void Close()
        {
            if ((MyForm != null))
            {
                MyForm.Close();
            }
        }

        private void Release()
        {
            ReleaseBase();
        }

        protected void ReleaseBase()
        {
            try
            {
                Dispose();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                m_oApp.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
            try
            {
                Application.SBO_Application.RightClickEvent -= new SAPbouiCOM._IApplicationEvents_RightClickEventEventHandler(SBO_Application_RightClickEvent);
                Application.SBO_Application.ItemEvent -= new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
                Application.SBO_Application.FormDataEvent -= new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBO_Application_FormDataEvent);
                Application.SBO_Application.MenuEvent -= new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
                m_oForm = null;
                //m_oApp = null;
                GC.Collect();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                m_oApp.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        /// <summary>
        /// Get a DBDataSource by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected SAPbouiCOM.DBDataSource MyDBDataSources(int index)
        {

            try
            {
                return MyForm.DataSources.DBDataSources.Item(index);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (!(MyForm == null))
                {
                    MyForm.Freeze(false);
                }

                return null;
            }


        }

        /// <summary>
        /// Get a DBDataSource by UniqueID
        /// </summary>
        /// <param name="datasourceID"></param>
        /// <returns></returns>
        protected SAPbouiCOM.DBDataSource MyDBDataSources(string datasourceID)
        {

            try
            {
                return MyForm.DataSources.DBDataSources.Item(datasourceID);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Application.SBO_Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;


        }

        /// <summary>
        /// Get a User DataSource by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected SAPbouiCOM.UserDataSource MyUserDataSources(int index)
        {
            try
            {
                return MyForm.DataSources.UserDataSources.Item(index);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;
        }

        /// <summary>
        /// Get a User DataSource by UniqueID
        /// </summary>
        /// <param name="datasourceID"></param>
        /// <returns></returns>
        protected SAPbouiCOM.UserDataSource MyUserDataSources(string datasourceID)
        {
            try
            {
                return MyForm.DataSources.UserDataSources.Item(datasourceID);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Application.SBO_Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;

        }

        /// <summary>
        /// Get a DataTable by index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected SAPbouiCOM.DataTable MyDataTables(int index)
        {
            try
            {
                return MyForm.DataSources.DataTables.Item(index);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;
        }

        /// <summary>
        /// Get a DataTable by UniqueID
        /// </summary>
        /// <param name="datasourceID"></param>
        /// <returns></returns>
        protected SAPbouiCOM.DataTable MyDataTables(string datasourceID)
        {
            try
            {
                return MyForm.DataSources.DataTables.Item(datasourceID);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Application.SBO_Application.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                if ((MyForm != null))
                    MyForm.Freeze(false);
            }
            return null;

        }

    }
}
