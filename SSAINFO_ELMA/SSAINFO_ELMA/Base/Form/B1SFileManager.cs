﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace SSAINFO_ELMA.Base.Form
{
    public static class B1SNodeConstant
    {
        public static readonly string ErrorNoB1S = "b1s file is not found!";
        public static readonly string AddFormNodePath = "/Application/forms/action[@type='add']/form";
        public static readonly string UpdateFormNodePath = "/Application/forms/action[@type='update']/form";
        public static readonly string AddItemInFormUpdateNodePath = "/Application/forms/action[@type='update']/form/items/action[@type='add']/item";
        public static readonly string ATTR_UID = "uid";
        public static readonly string ATTR_ClientH = "client_height";
        public static readonly string ATTR_ClientW = "client_width";
        public static readonly string ATTR_TOP = "top";
        public static readonly string ATTR_LEFT = "left";
        public static readonly string ATTR_W = "width";
        public static readonly string ATTR_FORM_TYPE = "FormType";
        public static readonly string B1S_POSTFIX = ".b1s";
        public static readonly string VSI = "VSIcreated";
    }

    internal class B1SFileManagerEx
    {
        private string m_b1sPackageName;
        private bool m_cleanupOnInitializeFailed;
        private XmlDocument m_b1sDoc;
        private XmlElement m_projectsHP;

        public string B1sPackageName
        {
            get
            {
                return this.m_b1sPackageName;
            }
            set
            {
                this.m_b1sPackageName = value;
            }
        }

        public bool RebuildOnInitializeFailed
        {
            get
            {
                return this.m_cleanupOnInitializeFailed;
            }
            set
            {
                this.m_cleanupOnInitializeFailed = value;
            }
        }

        public B1SFileManagerEx()
        {
            this.B1sPackageName = string.Empty;
        }

        public bool Initialize()
        {
            if (this.B1sPackageName == null || this.B1sPackageName == string.Empty)
                return false;
            this.m_b1sDoc = new XmlDocument();
            if (File.Exists(this.B1sPackageName))
            {
                string xml = File.ReadAllText(this.B1sPackageName);
                if (xml == string.Empty)
                    return false;
                try
                {
                    this.m_b1sDoc.LoadXml(xml);
                    this.m_projectsHP = this.m_b1sDoc.SelectSingleNode("projects") as XmlElement;
                    if (this.m_projectsHP != null)
                        return true;
                    if (this.m_cleanupOnInitializeFailed)
                        return false;
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    if (!this.m_cleanupOnInitializeFailed)
                        return false;
                }
            }
            this.m_b1sDoc = new XmlDocument();
            this.m_b1sDoc.AppendChild((XmlNode)this.m_b1sDoc.CreateXmlDeclaration("1.0", "utf-16", "yes"));
            this.m_projectsHP = this.m_b1sDoc.CreateElement("projects");
            this.m_b1sDoc.AppendChild((XmlNode)this.m_projectsHP);
            return true;
        }

        public bool GetProjectList(out List<string> projNameList, out List<string> projTypeList)
        {
            projNameList = new List<string>();
            projTypeList = new List<string>();
            if (this.m_b1sDoc == null)
                return false;
            foreach (XmlNode selectNode in this.m_b1sDoc.SelectNodes("projects/project"))
            {
                XmlNode namedItem1 = selectNode.Attributes.GetNamedItem("name");
                XmlNode namedItem2 = selectNode.Attributes.GetNamedItem("type");
                if (namedItem1 != null && namedItem2 != null)
                {
                    string str1 = namedItem1.Value;
                    string str2 = namedItem2.Value;
                    projNameList.Add(str1);
                    projTypeList.Add(str2);
                }
            }
            return true;
        }

        public bool GetFileList(string projName, out List<string> fileNameList, out List<string> fileTypeList)
        {
            fileNameList = new List<string>();
            fileTypeList = new List<string>();
            if (this.m_b1sDoc == null || projName == null || projName == string.Empty)
                return false;
            XmlNode xmlNode = this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']");
            if (xmlNode == null)
                return true;
            foreach (XmlNode selectNode in xmlNode.SelectNodes("file"))
            {
                XmlNode namedItem1 = selectNode.Attributes.GetNamedItem("name");
                XmlNode namedItem2 = selectNode.Attributes.GetNamedItem("type");
                if (namedItem1 != null && namedItem2 != null)
                {
                    string str1 = namedItem1.Value;
                    string str2 = namedItem2.Value;
                    fileNameList.Add(str1);
                    fileTypeList.Add(str2);
                }
            }
            return true;
        }

        public bool GetFileContent(string fullName, out string fileContent)
        {
            fileContent = (string)null;
            string[] strArray = fullName.Split('\\');
            if (strArray.Length != 2)
                return false;
            return this.GetFileContent(strArray[0], strArray[1], out fileContent);
        }

        public string GetRecordFileTime(string projName, string fileName)
        {
            string str = (string)null;
            if (string.IsNullOrEmpty(projName) || string.IsNullOrEmpty(fileName) || this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']") == null)
                return str;
            XmlNode fileNode = this.GetFileNode(projName, fileName);
            if (fileNode != null && fileNode.Attributes["time"] != null)
                str = fileNode.Attributes["time"].Value;
            return str;
        }

        public bool GetFileContent(string projName, string fileName, out string fileContent)
        {
            fileContent = (string)null;
            XmlNode fileNode = this.GetFileNode(projName, fileName);
            if (fileNode == null)
                return false;
            XmlNode firstChild = fileNode.FirstChild;
            if (firstChild == null)
                return false;
            XmlNode namedItem = firstChild.Attributes.GetNamedItem("desc");
            if (namedItem == null)
                return false;
            fileContent = namedItem.Value;
            return true;
        }

        private XmlNode GetFileNode(string projName, string fileName)
        {
            if (string.IsNullOrEmpty(projName) || string.IsNullOrEmpty(fileName))
                return (XmlNode)null;
            if (this.m_b1sDoc == null || this.m_projectsHP == null)
                return (XmlNode)null;
            return this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']/file[@name='" + fileName + "']");
        }

        public bool GetFileMetaData(string projName, string fileName, string attrName, out string attrValue)
        {
            attrValue = string.Empty;
            XmlNode fileNode = this.GetFileNode(projName, fileName);
            if (fileNode == null)
                return false;
            XmlNode namedItem = fileNode.Attributes.GetNamedItem(attrName);
            if (namedItem == null)
                return false;
            attrValue = namedItem.Value;
            return true;
        }

        public bool CreateNewProject(string projName, string projType)
        {
            if (this.m_b1sDoc == null || this.ContainsProject(projName))
                return false;
            XmlElement element = this.m_b1sDoc.CreateElement("project");
            element.SetAttribute("name", projName);
            element.SetAttribute("type", projType);
            this.m_projectsHP.AppendChild((XmlNode)element);
            return true;
        }

        public void RecordFileTime(string projName, string fileName, string fullFileName)
        {
            if (string.IsNullOrEmpty(projName) || string.IsNullOrEmpty(fileName) || this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']") == null)
                return;
            XmlNode fileNode = this.GetFileNode(projName, fileName);
            if (fileNode == null)
                return;
            if (fileNode.Attributes["time"] == null)
                fileNode.Attributes.Append(this.m_b1sDoc.CreateAttribute("time"));
            fileNode.Attributes["time"].Value = File.GetLastWriteTime(fullFileName).ToString();
        }

        public bool CreateNewFile(string projName, string fileName, string fileType)
        {
            if (string.IsNullOrEmpty(projName) || string.IsNullOrEmpty(fileName))
                return false;
            XmlNode xmlNode = this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']");
            if (xmlNode == null || this.ContainsFile(projName, fileName))
                return false;
            XmlElement element = this.m_b1sDoc.CreateElement("file");
            element.SetAttribute("name", fileName);
            element.SetAttribute("type", fileType);
            xmlNode.AppendChild((XmlNode)element);
            return true;
        }

        public bool RenameProjectName(string origProjName, string newProjName)
        {
            if (this.m_b1sDoc == null)
                return false;
            if (origProjName == newProjName)
                return true;
            XmlNode xmlNode = this.m_projectsHP.SelectSingleNode("project[@name='" + origProjName + "']");
            if (xmlNode == null)
                return false;
            (xmlNode as XmlElement).SetAttribute("name", newProjName);
            return true;
        }

        public bool RenameFileName(string projName, string origName, string newName)
        {
            if (this.m_b1sDoc == null)
                return false;
            if (origName == newName)
                return true;
            XmlNode xmlNode1 = this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']");
            if (xmlNode1 == null)
                return false;
            string xpath = "file[@name='" + origName + "']";
            XmlNode xmlNode2 = xmlNode1.SelectSingleNode(xpath);
            if (xmlNode2 == null)
                return false;
            (xmlNode2 as XmlElement).SetAttribute("name", newName);
            return true;
        }

        public bool SetFileContent(string projName, string fileName, string content)
        {
            if (string.IsNullOrEmpty(content))
                return false;
            XmlNode fileNode = this.GetFileNode(projName, fileName);
            if (fileNode == null)
                return false;
            XmlNode xmlNode = fileNode.SelectSingleNode("content");
            XmlElement xmlElement = xmlNode != null ? xmlNode as XmlElement : this.m_b1sDoc.CreateElement("content");
            xmlElement.SetAttribute("desc", content);
            fileNode.AppendChild((XmlNode)xmlElement);
            return true;
        }

        public bool SetFileMetaData(string projName, string fileName, string attrName, string attrValue)
        {
            if (string.IsNullOrEmpty(attrName))
                return false;
            if (attrValue == null)
                attrValue = string.Empty;
            XmlNode fileNode = this.GetFileNode(projName, fileName);
            if (fileNode == null)
                return false;
            (fileNode as XmlElement).SetAttribute(attrName, attrValue);
            return true;
        }

        public bool DeleteFile(string projName, string fileName)
        {
            XmlNode xmlNode = this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']");
            if (xmlNode == null)
                return false;
            string xpath = "file[@name='" + fileName + "']";
            XmlNode oldChild = xmlNode.SelectSingleNode(xpath);
            if (oldChild == null)
                return false;
            xmlNode.RemoveChild(oldChild);
            return true;
        }

        public bool SaveAllFiles()
        {
            if (this.B1sPackageName != null)
            {
                if (!(this.B1sPackageName == string.Empty))
                {
                    try
                    {
                        int length = this.B1sPackageName.LastIndexOf('\\');
                        if (length > 0)
                        {
                            string path = this.B1sPackageName.Substring(0, length);
                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);
                        }
                        using (FileStream fileStream = new FileStream(this.B1sPackageName, FileMode.Create, FileAccess.Write, FileShare.Write))
                            this.m_b1sDoc.Save((Stream)fileStream);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return true;
                }
            }
            return false;
        }

        public bool DeleteProject(string projName, string projType)
        {
            if (this.m_b1sDoc == null)
                return false;
            XmlNode oldChild = this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']");
            if (oldChild == null)
                return false;
            this.m_projectsHP.RemoveChild(oldChild);
            return true;
        }

        public bool ContainsFile(string projName, string fileName)
        {
            return this.GetFileNode(projName, fileName) != null;
        }

        public bool ContainsProject(string projName)
        {
            return this.m_projectsHP.SelectSingleNode("project[@name='" + projName + "']") != null;
        }

        public void RemoveAllProejects()
        {
            this.m_projectsHP.RemoveAll();
        }

        public bool Duplicate(string destFile, bool bOverwrite = true)
        {
            if (File.Exists(destFile))
            {
                if (!bOverwrite)
                    return false;
            }
            try
            {
                using (FileStream fileStream = new FileStream(destFile, FileMode.Create))
                    this.m_b1sDoc.Save((Stream)fileStream);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return false;
            }
            return true;
        }
    }
}
